# SCD30 + LPS2x + PCF8523
## no wifi, so it starts up faster!

This version is similar to the "all the bells and whistles" version except that it doesn't poll AdafruitIO for the current time. Because you don't have to wait for an Internet connection to be established, it takes less time to initialize.

* If you're missing either LPS2x or PCF8523, this sketch should fail gracefully and fall back to manually set variables. The only hardware you must have is the Pyportal and the SCD-30.

This sketch requires the following libraries:

* time
* board
* neopixel
* busio
* adafruit_scd30
* terminalio
* displayio
* adafruit_datetime
* adafruit_lps2x
* adafruit_pcf8523
* adafruit_sdcard
* storage
* os
* adafruit_pyportal
* adafruit_display_text.label
* adafruit_bitmap_font

(in the event that you find a stray library that is acutally unused, by all means submit a PR to remove it)
