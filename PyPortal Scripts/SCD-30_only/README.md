# SCD-30 Only #

This script uses only on-board libraries and the SCD-30 sensor. It displays the CO2, temperature, and rH with color changes at set breakpoints. The CO2 breakpoints are easily settable at program-time.

This script does not use:
  * Pressure sensor to improve accuracy
  * RTC to provide current time information
  * Logging to SD card
  * Wifi or Bluetooth

Use this script when you only have a CO2/temp/rH sensor (script assumes SCD-30) and you'll be in a location where Internet access via WiFi is not available.

Prerequisites: everything listed in the README that is in the directory root, plus `adafruit_scd30` (assuming you're using the Adafruit SCD-30, otherwise you'll need another library and may need to modify the script)
