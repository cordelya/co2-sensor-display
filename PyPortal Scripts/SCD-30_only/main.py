import time
import board
import neopixel
import busio
import adafruit_scd30
import terminalio
import displayio
from adafruit_display_text.label import Label
from adafruit_bitmap_font import bitmap_font

# set up some variables
PYPORTAL_REFRESH = 5 # how long the script will wait between sensor data fetches
green = 0x03fc2c # the color to use for "good" levels
yellow = 0xf0fc03 # the color to use for "almost bad" levels
red = 0xfc0303 # the color to use for "bad" levels
big_font = bitmap_font.load_font('/fonts/Nunito-Light-75.bdf')
little_font = bitmap_font.load_font('/fonts/Nunito-Black-17.bdf')
text_color = green # initially sets the text color to green
DISPLAY_HEIGHT = 240 # the PyPortal's height is 240 px
DISPLAY_WIDTH = 320 # the PyPortal's width is 320 px

# set desired CO2 ranges. The "yellow" range will be the values between the HIGH and the LOW
CO2_HIGH = 1200 # the lowest PPM of the "high" range
CO2_LOW = 800 # the highest PPM of the "low" range

# initialize things
i2c = busio.I2C(board.SCL, board.SDA, frequency=50000)
sensor = adafruit_scd30.SCD30(i2c)
status_light = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=1) # an easy reference to the neopixel
display = board.DISPLAY # initialize the display
little_font.load_glyphs(b'01234567890TemprH:.%°') # preload glyphs we know we're going to use
big_font.load_glyphs(b'01234567890COPPM:.') # preload glyphs we know we're going to use

# set up group and labels
# If you modify the initial text, make sure you include enough padding to accommodate sensor data
# because once you initialize a label, the max size is what you originally used!
splash = displayio.Group() # create the main display group
co2_content = ' ' # initially we don't have any data, so we'll leave it blank (padding gets added next line)
co2_text = Label(big_font, text=co2_content*30, color = text_color) # initialize the CO2 reading label
co2_text.anchor_point = (0.5, 0.0) # use the upper middle point of the co2_text label for positioning
co2_text.anchored_position = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 0.1) # position the label at this location
splash.append(co2_text) # add this label to the main group
ppm = Label(little_font, text='PPM', color=text_color) # initialize the PPM label
ppm.anchor_point = (0.5,0.0) # position using the upper middle point
ppm.anchored_position = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 0.5)
splash.append(ppm) # add to the main group
content='Temp: / rH:'+' '*15 # we don't have readings yet, so let's put what we do have, plus padding for later
other_text = Label(little_font, text=content, color=text_color) # initialize temp/rH label
other_text.anchor_point = (0.5,1.0) # use center bottom point for positioning
other_text.anchored_position = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 0.95) # position in this location
splash.append(other_text) # add to main group
display.show(splash) # show the group as currently configured

# give the sensor a few seconds to start up
time.sleep(3)

# wait for the sensor to report that it has a reading, then store the readings into local variables
while sensor.data_available:
    # read the temperature sensor, convert to F, and trim to 1 decimal place
    temperature = round((sensor.temperature * 9 / 5) + 32, 1)
    co2 = sensor.CO2 # store the CO2 reading
    humidity = round(sensor.relative_humidity, 1) # store the rH reading and trim to 1 decimal place
    # determine what color we should use based on CO2 concentration
    if co2 > CO2_HIGH:
        text_color = red
        status_light.fill(red)
    elif co2 > CO2_LOW:
        text_color = yellow
        status_light.fill(yellow)
    else:
        text_color = green
        status_light.fill(green)
    co2_text.text = str(int(co2)) # update the co2_text label with the fresh data
    co2_text.color = text_color # update the co2_text label with the correct color
    ppm.color = text_color # update the ppm label with the correct color
    other_text.text = 'Temp: '+ str(temperature) + '°F / rH: ' + str(humidity) +'%' # update the temp/rH label
    other_text.color = text_color # update the temp/rH label color
    display.refresh() # refresh the display to show the new information
    time.sleep(PYPORTAL_REFRESH) # sleep until next update interval
