# Using a PyPortal #

If you want to use a PyPortal with any of these scripts, you will probably want to modify your PyPortal's 5v/3.3v selector so that 3.3v is selected. The PyPortal has a known quirk where if you try to run a PyPortal with 5v selected and sensors connected to the I2C, it will at best have a temper tantrum for a few minutes before booting, and at worst, refuse to boot up at all. 

TODO: Link to the Adafruit PyPortal info page that provides more info about modifying the voltage selector.

Required libraries will be listed in each script's README.

If you don't have the fonts named in the script, you will need to obtain two font files - one large and one small - to emulate the sizing shown.
You will then need to copy those font bitmap files into the fonts folder and adjust the variables `big_font` and `little_font` accordingly.

You can set your own color change thresholds by modifying the `CO2_HIGH` and `CO2_LOW` variables. `CO2_HIGH` should be set to the lower end of the "red" range and `CO2_LOW` should be set to the upper end of the "green" range. The "yellow" range will automatically fall in between these two values.

You can set your own color values by modifying variables `green`, `yellow`, and `red`.

Set pressure manually (SCD30 uses it to be more accurate) by editing `pressure` 

Set an estimated start date/time by editing `rtc_time` (near line 54)

Connect the sensor to the board (I needed a Stemma/QT to 4-pin JST cable for this), drop `main.py` into the top level folder and copy the appropriate libraries into the `lib` folder. 

![Three vertically stacked photos of a PyPortal with information displayed (CO2 reading, temperature, and rH) on each](/source/images/pyportal-co2-monitor-smol.jpg "CO2 Monitor Demo")
