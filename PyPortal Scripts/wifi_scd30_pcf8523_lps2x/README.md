# CO2 Sensor Logger
## With pressure sensor lps22 or lps25, RTC pcf8523, SD card, and available wifi

This sketch does it all: 

If you're missing either LPS2x or PCF8523, this sketch should fail gracefully and fall back to manually set variables. The only hardware you must have is the Pyportal and the SCD-30.

If no wifi is available, this sketch will still try to connect 10 times before it gives up, but it should fail gracefully and not prevent the rest of the sketch from running.

* tries to fetch the current time in UTC: 
    * first from the Internet (requires a free AdafruitIO account)
    * then from an external RTC, if attached
    * then fails back to a manually set variable you can set ahead of time
* finds the next available sequential log file number and initializes the new log file with column headers
    * If any part of this process fails, the "logging" flag will not be set to True and no logging will occur
* updates the SCD-30 with accurate local pressure (once, at start)
    * fails back to average pressure at sea level or manually set value in millibars
* calculates the difference in temperature reading from the SCD-30 and the LPS2x 
    * later applies that difference (or a pre-set variable if not) to compensate for the SCD-30 running hot
* displays the current readings, refreshing at an inteval set in the PYPORTAL_REFRESH constant
* changes the color of the text and neopixel at pre-set intervals (easily change these, and the colors at the top of the sketch)
* appends current readings to log file for analysis later on

Required libraries:

* time
* board
* neopixel
* busio
* adafruit_scd30
* terminalio
* displayio
* adafruit_datetime
* adafruit_lps2x
* adafruit_pcf8523
* adafruit_sdcard
* storage
* adafruit_pyportal
* os
* adafruit_display_text
* adafruit_bitmap_font

In the event that you identify a stray library in this list that actually isn't a dependency, you are cordially invited to submit a PR to remove it :)
