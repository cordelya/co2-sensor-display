import time
import board
import neopixel
import busio
import adafruit_scd30
import terminalio
import displayio
import adafruit_datetime
import adafruit_lps2x
import adafruit_pcf8523
import adafruit_sdcard
import storage
import adafruit_pyportal
import os
from adafruit_display_text.label import Label
from adafruit_bitmap_font import bitmap_font

# debug flag
debug = 0

# set desired CO2 ranges. The "yellow" range will be the values between the HIGH and the LOW
CO2_HIGH = 1200 # the lowest PPM of the "high" range
CO2_LOW = 800 # the highest PPM of the "low" range

# customize text and neopixel colors
green = 0x03fc2c # the color to use for low ("good") levels
yellow = 0xf0fc03 # the color to use for medium ("almost bad") levels
red = 0xfc0303 # the color to use for high ("bad") levels

# how long should we wait between sensor reads, in seconds
PYPORTAL_REFRESH = 5

# display stuff
# you can change the fonts by dropping other font files and changing the names here
big_font = bitmap_font.load_font('/fonts/Nunito-Light-75.bdf')
little_font = bitmap_font.load_font('/fonts/Nunito-Black-17.bdf')
text_color = green # initially sets the text and neopixel color to green
DISPLAY_HEIGHT = 240 # the PyPortal's height is 240 px
DISPLAY_WIDTH = 320 # the PyPortal's width is 320 px

# if SD card is available, create object and initialize a logfile
logging = False # only log readings if SD card initializes okay
filename = False # eventually will contain the filename

# manual settings for sensor adjustment, when you don't have attachments

# if you know the predicted pressure for the location, you can set it here
pressure = 1013.25 # 1013.25 is average air pressure at sea level in millibars (mB)

# if you don't have an LPS22, you can set a number of degrees C
# to subtract from SCD30, which tends to run/read a few degrees hot
temperature_delta = 0 # positive int or float of degrees to subtract from SCD30
# if you won't have Internet access or a RTC, you can estmate your start time
rtc_time = time.struct_time(tm_year=2021, tm_mon=9, tm_mday=5, tm_hour=16, tm_min=30,
    tm_sec=0, tm_wday=-1, tm_yday=-1, tm_isdst=-1)

# set up some helper variables
time_delta = False # stores time.monotonic() for later delta calculation
date_time = False # initialize for time handling
t = False # initialize for time handling

# initialize hardware and display items
pyportal = adafruit_pyportal.PyPortal(debug = True, )
i2c = busio.I2C(board.SCL, board.SDA, frequency=50000)
sensor = adafruit_scd30.SCD30(i2c)
status_light = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=1) # an easy reference to the neopixel
display = board.DISPLAY # initialize the display
little_font.load_glyphs(b'01234567890TemprH:.%°') # preload glyphs we know we're going to use
big_font.load_glyphs(b'01234567890COPPM:.') # preload glyphs we know we're going to use

# timekeeping: try to get internet time, try RTC, or fail back to manually set time
# if a wireless connection is available, query the current time.
try:
    pyportal.get_local_time(location="Etc/UTC")
    rtc_time = time.localtime()
    time_delta = time.monotonic()
except:
    print("I couldn't retrieve the current time from the Internet")
    # if RTC is available, create object and update pressure variables
    try:
        rtc = adafruit_pcf8523.PCF8523(i2c)
        rtc_time = rtc.datetime # expressed as struct_time
        time_delta = time.monotonic()
    except:
        time_delta = time.monotonic()
        print("I couldn't get the current time from a RTC")

# Prepare for SD card logging
# Get next incremental filename here and re-assign filename
# If this fails, logging will not occur, but display will continue
i = 0 # initialize filename counter
try:
    y = 0 # counter for traversing the file list
    sd_dir = os.listdir("/sd/") # get the list of files
    sd_dir.sort() # sort the file list so the filenames are in order
    if debug == 1:
        print(sd_dir)
    while y < len(sd_dir): #iterate over the file list looking for filename matches
        if debug == 1:
            print("Current file is " + sd_dir[y])
        while sd_dir[y] == ("log%03d.csv" % i):
            if debug == 1:
                print("Found log%03d.csv" % i)
            i += 1 # we found a match, so increment the filename counter
        y += 1 # increment the list iterator to advance to the next filename
except:
        print("Could not read the next file number")
filename = ("/sd/log%03d.csv" % i) # create the filename from the counter
try:
    with open(filename, "w+") as f:
        f.write("datetime, co2, temp, humidity, pressure\n")
    logging = True
    if debug == 1:
        print(filename + " initialized")
except:
    print("Could not initialize file.")

# if LPS22 is available, create object and update the SCD30
# we'll also check for a temperature difference and set a delta
try:
    pressure_sen = adafruit_lps2x.LPS22(i2c)
    sensor.ambient_pressure = pressure_sen.pressure #set SCD30 pressure variable
    temperature_delta = sensor.temperature - pressure_sen.temperature
    if debug == 1:
        print("Updated the CO2 sensor's pressure input")
except:
    sensor.ambient_pressure = pressure #average pressure at sea level; fallback default
    print("I couldn't connect to a pressure sensor, so I set the CO2 sensor's reference pressure to the default")

# set up group and labels
# If you modify the initial text, make sure you include enough padding to accommodate sensor data
# because once you initialize a label, the max size is what you originally used!
splash = displayio.Group() # create the main display group
if debug == 1:
    print("display group created!")
co2_content = ' ' # initially we don't have any data, so we'll leave it blank (padding gets added next line)
co2_text = Label(big_font, text=co2_content*30, color = text_color) # initialize the CO2 reading label
co2_text.anchor_point = (0.5, 0.0) # use the upper middle point of the co2_text label for positioning
co2_text.anchored_position = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 0.1) # position the label at this location
splash.append(co2_text) # add this label to the main group
if debug == 1:
    print("CO2 text label added!")
ppm = Label(little_font, text='PPM', color=text_color) # initialize the PPM label
ppm.anchor_point = (0.5,0.0) # position using the upper middle point
ppm.anchored_position = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 0.5)
splash.append(ppm) # add to the main group
if debug == 1:
    print("PPM text label added!")
content='Temp: / rH:'+' '*15 # we don't have readings yet, so let's put what we do have, plus padding for later
other_text = Label(little_font, text=content, color=text_color) # initialize temp/rH label
other_text.anchor_point = (0.5,1.0) # use center bottom point for positioning
other_text.anchored_position = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 0.95) # position in this location
splash.append(other_text) # add to main group
if debug == 1:
    print("temp & rH label added!")
display.show(splash) # show the group as currently configured

# if we don't have a short pause here, the sketch sometimes just stops
time.sleep(2)

# wait for the sensor to report that it has a reading, then store the readings into local variables
while sensor.data_available:
    # convert the temperature reading to F, and trim to 1 decimal place
    temperature = round(((sensor.temperature - temperature_delta)* 9 / 5) + 32, 1)
    co2 = sensor.CO2 # store the CO2 reading
    humidity = round(sensor.relative_humidity, 1) # store the rH reading and trim to 1 decimal place
    # determine what color we should use based on CO2 concentration
    if co2 > CO2_HIGH:
        text_color = red
        status_light.fill(red)
    elif co2 > CO2_LOW:
        text_color = yellow
        status_light.fill(yellow)
    else:
        text_color = green
        status_light.fill(green)
    co2_text.text = str(int(co2)) # update the co2_text label with the fresh data
    co2_text.color = text_color # update the co2_text label with the correct color
    ppm.color = text_color # update the ppm label with the correct color
    other_text.text = 'Temp: '+ str(temperature) + '°F / rH: ' + str(humidity) +'%' # update the temp/rH label
    other_text.color = text_color # update the temp/rH label color
    display.refresh() # refresh the display to show the new information
    if rtc_time != False:
        current = (time.monotonic())
        seconds = (current - time_delta)
        date_time = (time.mktime(rtc_time) + int(seconds))
        t = time.localtime(date_time)
    else:
        date_time = time.monotonic()
    if logging == True:
        datestring = '%04d-%02d-%02d_%02d:%02d:%02d' % (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec)
        with open(filename, "a") as f:
            f.write(str(datestring) + ", " + str(co2) + ", " + str(temperature) + ", " + str(humidity) + ", " + str(pressure_sen.pressure) +"\n")
    time.sleep(PYPORTAL_REFRESH) # sleep until next update interval
