"""Report Sensor Data to Prometheus"""

# Import Modules
from prometheus_client import Gauge, start_http_server
import time
import board
import adafruit_scd30
import adafruit_lps2x
import logging
#from systemd.journal import JournalHandler

# Define Constants
DEBUG = False
SCD30_REFRESH = 10  # SCD30 poll interval, in seconds
LPS22_REFRESH = 30  # LPS22 poll interval, in seconds

# Define Functions
def poll_scd30(pressure):
    # Wait for data_available to be True
    while scd30.data_available == False:
        pass

    # Read the sensor & return the value
    try:
        co2 = scd30.CO2
        rh = scd30.relative_humidity
        scd30_time = time.monotonic()
        if DEBUG:
            print("CO2: {} | rH: {}".format(co2, rh))
        return co2, rh
    except:
        print("Reading the SCD30 failed")


def poll_lps22():
    # Poll the sensor
    try:
        pressure = lps22.pressure
        tc = lps22.temperature

        # Update the SCD30's Reference Pressure Value
        scd30.ambient_pressure = pressure

        lps22_time = time.monotonic()
        if DEBUG:
            print("Temperature: {} | Pressure: {}".format(tc, pressure))
        # Return the values: 'pressure', 'tc', 'humidity'
        return pressure, tc
    except:
        print("Reading the LPS22 failed")

def c_to_f(temp):
    return round(((temp * 9/5) + 32), 1)

def update_prom(co2, tf, tc, humidity, pressure):
    g_co2.set(co2)
    g_temp.labels('celcius').set(tc)
    g_temp.labels('fahrenheit').set(tf)
    g_humidity.set(humidity)
    g_pressure.set(pressure)

# Set up Logging
#log = logging.getLogger('environmental_sensors')
#log.addHandler(JournalHandler())
#log.setLevel(logging.INFO)

# Initialize Sensors
try:
    scd30 = adafruit_scd30.SCD30(board.I2C())
    print("SCD-30 CO2 Sensor initialized")
except:
    print("SCD-30 CO2 Sensor could not be initialized")
try:
    lps22 = adafruit_lps2x.LPS22(board.I2C())
    print("LPS22 Pressure Sensor initialized")
except:
    print("LPS22 Pressure Sensor could not be initialized")

# Initialize Prometheus Gauges
g_humidity = Gauge(
        'lps22_humidity_percent',
        'Humidity percentage measured by the LPS22 Sensor'
        )
g_co2 = Gauge('scd30_co2', 'CO2 PPM measured by the SCD30 Sensor')
g_temp = Gauge(
        'lps22_temp',
        'Temperature measured by the LPS22 Sensor',
        ['scale']
        )
g_pressure = Gauge('lps22_pressure', 'Pressure measured by the LPS22 Sensor')

# Initialize labels for the Temperature Scale
g_temp.labels('celcius')
g_temp.labels('fahrenheit')

print("Prometheus Gauges Initialized")

# Main Loop

if __name__ == "__main__":
    print("Now entering Main")

    # Expose metrics
    metrics_port = 8000

    # Initialize pressure var to standard avg pressure at sea level
    pressure = 1013.25

    # Do initial sensor polling
    # poll LPS22 first because we will supply the pressure reading
    # to the SCD30 so it is more accurate
    pressure, tc = poll_lps22()
    co2, humidity = poll_scd30(pressure)
    print("Initial sensor polling completed")

    # Record time of last polling for each sensor
    scd30_time = time.monotonic()
    lps22_time = time.monotonic()

    start_http_server(metrics_port)
    print("Serving sensor metrics on :{}".format(metrics_port))
    #log.info("Serving sensor metrics on :{}".format(metrics_port))

    while True:
        if DEBUG:
            print("{} since last LPS22 Poll".format(time.monotonic() - lps22_time))
            print("{} since last SCD30 Poll".format(time.monotonic() - scd30_time))
        if (time.monotonic() - lps22_time) >= LPS22_REFRESH:
            pressure, tc = poll_lps22()
            lps22_time = time.monotonic()
        if (time.monotonic() - scd30_time) >= SCD30_REFRESH:
            co2, humidity = poll_scd30(pressure)
            scd30_time = time.monotonic()

        update_prom(co2, tc, c_to_f(tc), humidity, pressure)
