# Weather Station Script for Feather S3

## Intro

This script, when run on a Feather S3 board (by Unexpected Maker) which is connected to a PMSA003i particulate matter sensor and a BME680 temperature/pressure/humidity/voc sensor, will attempt to connect to wifi using provided credentials and then will attempt to connect to and publish regularly to an MQTT broker using provided credentials. You supply everything but the script ;)

This will report:

* Air temperature
* Relative humidity
* Atmospheric pressure
* VOC (this is largely meaningless as it requires training to detect specific materials and the proprietary library to analyze it is only available for Arduino scripts)
* PM 1.0, 2.5, and 10.0 readings
* Particles per 0.1 L air, by particle sizes < 0.3, < 0.5, < 1.0, < 5.0, < 10.0 micron 

What you do with the data after it reaches your MQTT broker is up to you ;)

## Hardware

This script was written for the following hardware configuration:

* [Feather S3 by Unexpected Maker](https://unexpectedmaker.com/shop/feathers3) $22
* [PMSA003i PM sensor with STEMMA QT I2C Connector](https://www.adafruit.com/product/4632) $45
* [BME680 sensor with STEMMA QT I2C Connector](https://www.adafruit.com/product/3660) $19
* 2 x [STEMMA QT cables](https://www.adafruit.com/product/4209) $1 each
* Enclosure (files provided, you handle the printing)
* USB-C cable (needs data capability only to program the Feather S3, otherwise only needs power)

## Externalities

* The SSID and password for an accessible Wifi router.
* Login credentials for an MQTT broker - must allow publishing

## Files 

Connect the sensors to the Feather S3 using the STEMMA QT cables. Connect the Feather S3 to your computer via USB cable. Drop both *.py files into the Feather S3's USB storage. Rename `weatherstation.py` file to `code.py`, and rename `secrets-example.py` to `secrets.py`. Edit `secrets.py` to specify your credentials.

If you need to debug or edit, you can refer to this guide for the [Adafruit ESP32 S3 Feather](https://learn.adafruit.com/adafruit-esp32-s3-feather/circuitpython), which is similar to the Feather S3.

The enclosure has pegs upon which each board can sit. I found I needed to add a dab of school glue to the mounting points to keep the boards from coming loose. There's a slot for the USB cable to exit the enclosure. You should connect the USB-C cable to the Feather S3 before closing the enclosure.
