"""Store secrets here so they're not in the main application

rename this file to secrets.py before use.
"""

secrets = {
    "ssid": "your wifi ssid",
    "password": "your wifi password",
    "broker": "your MQTT broker's address",
    "port": "your MQTT broker's port",
    "mqtt_user": "your MQTT broker username",
    "mqtt_pass": "your MQTT broker password"
}
