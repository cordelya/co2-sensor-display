"""Weather Station CircuitPython Script

Regularly polls connected sensors and reports readings to an MQTT broker.

Required hardware: 

* Feather S3 board from Unexpected Maker
* Adafruit PMSA003i Particulate Matter Sensor
* Adafruit BME680 temperature/pressure/humidity/voc sensor

Required externalities:
MQTT broker credentials (stored in secrets.py)
Wifi credentials (stored in secrets.py)
"""

import time
import board, digitalio
import feathers3
import adafruit_bme680
import ipaddress
import ssl
import wifi
import socketpool
import json
import adafruit_minimqtt.adafruit_minimqtt as MQTT
from adafruit_pm25.i2c import PM25_I2C

try:
    from secrets import secrets
except ImportError:
    print("Wifi & MQTT secrets are kept in secrets.py, please add them there!")
    raise

### Variables & Constants ###

# To turn on debugging printouts, change this to True
DEBUG = False


### Functions ###

# See: https://learn.adafruit.com/mqtt-in-circuitpython and https://docs.circuitpython.org/projects/minimqtt/en/latest/api.html

def connect(mqtt_client, userdata, flags, rc):
    # This function will be called when the mqtt_client is connected successfully to the broker.
    if DEBUG:
        print("Connected to MQTT Broker!")
        print("Flags: [0]\n RC [1]".format(flags, rc))

def disconnect(mqtt_client, userdata, rc):
    # This function is called when the mqtt_client disconnects from the broker.
    if DEBUG:
        print("Disconnected from MQTT Broker!")

def subscribe(mqtt_client, userdata, topic, granted_quos):
    # This function is called when the mqtt_client subscribes to a new feed
    if DEBUG:
        print("Subscribed to {0} with QOS level {1}".format(topic, granted_quos))

def unsubscribe(mqtt_client, userdata, topic, pid):
    # This function is called when the mqtt_client unsubscribes from a feed.
    if DEBUG:
        print("Unsubscribed from {0} with PID {1}".format(topic, pid))

def publish(mqtt_client, userdata, topic, pid):
    # This method is called when the mqtt_client publishes data to a feed.
    if DEBUG:
        print("Published to {0} with PID {1}".format(topic, pid))

def message(client, topic, message):
    # Method called when a client's subscribed feed has a new value.
    if DEBUG:
        print("New message on topic {0}: {1}".format(topic, message))


# Init board & sensors
i2c = board.I2C()
reset_pin = None
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c)
pmsa003i = PM25_I2C(i2c, reset_pin)

# Connect to Wifi
if DEBUG:
    print("Connecting to {}".format(secrets['ssid']))
wifi.radio.connect(secrets['ssid'], secrets['password'])
if DEBUG:
    print("Connected to {}!".format(secrets['ssid']))
    print("My IP address is {}".format(wifi.radio.ipv4_address))

# Create socket pool
pool = socketpool.SocketPool(wifi.radio)

# Set up a MiniMQTT Client
mqtt_client = MQTT.MQTT(
    broker=secrets["broker"],
    port=secrets["port"],
    username=secrets["mqtt_user"],
    password=secrets["mqtt_pass"],
    client_id="weatherstation",
    socket_pool=pool,
    is_ssl=True,
    ssl_context=ssl.create_default_context(),
)

# Connect callback handlers to mqtt_client
mqtt_client.on_connect = connect
mqtt_client.on_disconnect = disconnect
mqtt_client.on_subscribe = subscribe
mqtt_client.on_unsubscribe = unsubscribe
mqtt_client.on_publish = publish
mqtt_client.on_message = message

if DEBUG:
    print("Attempting to connect to %s" % mqtt_client.broker)
mqtt_client.connect()

if DEBUG:
    print("Publishing to test topic")
mqtt_client.publish("weather/test", "Hello Broker!")

while True:
    try:
        mqtt_client.loop()
        if DEBUG:
            print("Now serving up environmental sensor readings to the local MQTT broker")
        # Establish / Clear variables
        pm10 = ""
        pm25 = ""
        pm100 = ""
        part03um = ""
        part05um = ""
        part10um = ""
        part50um = ""
        part100um = ""
        temperature = ""
        gas = ""
        pressure = ""
        humidity = ""

        # Sensor readings wrapped in Try/Except clauses so a single
        # read failure won't gum up the works

        # Read the PMSA003I
        try:
            aqdata = pmsa003i.read()
            pm10 = aqdata["pm10 env"]
            pm25 = aqdata["pm25 env"]
            pm100 = aqdata["pm100 env"]
            part03um = aqdata["particles 03um"]
            part05um = aqdata["particles 05um"]
            part10um = aqdata["particles 10um"]
            part50um = aqdata["particles 50um"]
            part100um = aqdata["particles 100um"]
        except:
            pass

        # Read the BME680
        try:
            # Read the temperature and convert C to F
            temperature = (bme680.temperature * 9/5 + 32)
            gas = bme680.gas
            humidity = bme680.humidity
            pressure = bme680.pressure
        except:
            pass

        # Publish readings
        mqtt_client.publish("weather/outside/temperature", temperature, retain=True)
        mqtt_client.publish("weather/outside/gas", gas, retain=True)
        mqtt_client.publish("weather/outside/humidity", humidity, retain=True)
        mqtt_client.publish("weather/outside/pressure", pressure, retain=True)
        mqtt_client.publish("weather/outside/pm10", pm10, retain=True)
        mqtt_client.publish("weather/outside/pm25", pm25, retain=True)
        mqtt_client.publish("weather/outside/pm100", pm100, retain=True)
        mqtt_client.publish("weather/outside/part03um", part03um, retain=True)
        mqtt_client.publish("weather/outside/part05um", part05um, retain=True)
        mqtt_client.publish("weather/outside/part10um", part10um, retain=True)
        mqtt_client.publish("weather/outside/part50um", part50um, retain=True)
        mqtt_client.publish("weather/outside/part100um", part100um, retain=True)
    except (ValueError, RuntimeError) as e:
        wifi.reset()
        mqtt_client.reconnect()
        continue
    time.sleep(10)
