# Feather S3

The scripts in this directory are meant to be run on a Feather S3 (by Unexpected Maker).

Additional scripts for the Feather S3 may be found in the [CircuitPython Scripts](https://gitlab.com/cordelya/circuitpython-scripts) repo.
