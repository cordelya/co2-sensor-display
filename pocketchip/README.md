# PocketCHIP as CO2 Monitor Display

The script(s) in this directory are meant to be run on a PocketCHIP. Unless otherwise indicated, a separate device will handle the polling and then hand off the data via USB Serial. The PocketCHIP side only handles displaying that information on the screen.

The device passing the data via USB Serial should pass that data as JSON, with the following ids: CO2, temperature, pressure, humidity

One could use a Trinkey QT2040 for this and there's a tutorial on the Adafruit website that covers this exact topic.
