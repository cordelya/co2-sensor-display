'''Environmental Data Graphical Display

Depends on environmental data (CO2, temp, pressure, humidity) coming in via
USB Serial from, for example, a Trinkey QT2040. The Trinkey handles reading
the sensors and ships the resulting data out via USB serial. This script
reads the USB serial and uses the data to initialize and update a graphical
display using PySimpleGUI. Written for use with a PocketCHIP as the display.

License: MIT
'''

import json
import serial
import time
import datetime
import os
import sys
import sparklines
import PySimpleGUI as sg

# User Settings                                                      #
# ================================================================== #

# debug flag
debug = False

# CO2 Color-Coding Params
# Note: the yellow color will be used when the CO2 PPM is above
# CO2_LOW but below CO2_HIGH. We do not explicity set it.

CO2_HIGH = 1200 # above this, we use the red color scheme
CO2_LOW = 800 # below this, we use the green color scheme

green = '#03FC2C' # the hex color to use for low/good/green levels
yellow = '#F0FC03' # the hex color to use for med/yellow levels
red = '#FC0303' # the hex color to use for high/bad/red levels

DATA_REFRESH = 5 # how long should we wait between serial reads?

# PocketCHIP's display is 272px high x 480px wide
DISPLAY_HEIGHT = 272 # the screen's height in pixels
DISPLAY_WIDTH = 480 # the screen's width in pixels

# initialize some helper variables
time_delta = time.monotonic()
date_time = time.localtime()
t = False
repeat = True

# functions go here

# initialize the display
if os.environ.get('DISPLAY', '') == '':
        if debug:
                print('no display setting found. Using :0')
        os.environ.__setitem__('DISPLAY', ':0')

if debug:
        print("Display Set. Initializing PySimpleGUI")

# PySimpleGUI Init
layout = [
        [sg.Button('Exit'), sg.Text('', background_color = '#000000', expand_x = True, expand_y = True)],
        [sg.Text('CO2', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'co2', font = ('Nunito', 36, 'bold'))],
        [sg.Text('PPM', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'ppm', font = ('Nunito', 18))],
        [sg.Text('Sparkline', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'sparkline', font = ('Nunito', 18))],
        [sg.Text('Temp, pressure, humidity', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'other', font = ('Nunito', 16))]
]
window = sg.Window('CO2 Monitor', layout, background_color = '#000000', finalize = True, resizable=True, element_justification = 'center')
window.maximize()

if debug:
        print("PySimpleGUI window Initialized")

# open the USB serial port
ss = serial.Serial("/dev/ttyACM1")

# read the string from the serial port
_ = ss.readline() # toss the first read in case it is incomplete
raw_string = ss.readline().strip().decode()

if debug:
        print(raw_string)

# load the json string into a variable
data = json.loads(raw_string)

time_last = int(time.monotonic()) - 5

# init the Sparkline list
co2_list = []
for x in range(18):
        co2_list.append(float(800))
if debug:
        print("Now entering main loop")

while True:

        # check for exit button press
        event, values = window.read(timeout = 5)
        if event == sg.WIN_CLOSED or event == 'Exit':
                os._exit(1)
                break
        if debug:
                print("Event check completed. Read from Serial is up next.")
        # read from the serial and update the window
        try:
                raw_string = ss.readline().strip().decode()
                if debug:
                        print(raw_string)
                data = json.loads(raw_string)
                co2_list.append(float(data['CO2']))
                if len(co2_list) > 18:
                        co2_list.pop(0) # trim the sparkline list by removing the oldest item
                if data['CO2'] > CO2_HIGH:
                        t_color = red
                elif data['CO2'] > CO2_LOW:
                        t_color = yellow
                else:
                        t_color = green

                co2_text = str(data['CO2'])
                sparkline_list = sparklines.sparklines(co2_list)
                sparkline_text = " ".join([str(item) for item in sparkline_list])
                other_text = '{}F | {}% rH | {} mBar'.format(data['temperature'], data['humidity'], data['pressure'])
                window['co2'].update(value = str(co2_text), text_color = t_color)
                window['ppm'].update(text_color = t_color)
                window['sparkline'].update(value = str(sparkline_text), text_color = t_color)
                window['other'].update(value = str(other_text), text_color = t_color)
                window.refresh()
                window.maximize()

                time_last = int(time.monotonic())
                if debug:
                        print("End of main loop")
        except KeyboardInterrupt:
                window.close()
                os._exit(1)
                break
        except:
                exception_type, exception_object, exception_traceback = sys.exc_info()
                error_file = exception_traceback.tb_frame.f_code.co_filename
                line_number = exception_traceback.tb_lineno
                print("Hmm... that didn't work as expected.")
                print("Exception: ", exception_type)
                print("File: ", error_file)
                print("Line: ", line_number)
        finally:
                pass
        if repeat == False:
                window.close()
                os._exit(1)
                break
        else:
                continue
        os._exit(1)
        break

sys.exit()
