# Sensor Logging and Display Script 
# by Cordelya Sharpe
# based on, in part, several examples at https://learn.adafruit.com
# Raspberry Pi + Official 7" Touchscreen Edition
# Licensed under the MIT License. See https://gitlab.com/cordelya/co2-sensor-display

import time
import datetime
import board
import busio
import adafruit_scd30
import adafruit_lps2x
import adafruit_pcf8523
import os
import sys
import getopt
import pygame
import sparklines
import qwiic_led_stick as ledstick
from ft5406 import Touchscreen, TS_PRESS, TS_RELEASE, TS_MOVE

debug = False
logging = True
location = "unknown"
pressure = 1013.25 # the default pressure to use. 1013.25 is standard air pressure at sea level
filename = True
grafana = False # grafana logging set to false by default. To turn on, pass a location using -l [location] when starting this script
logfile_path = "/home/pi/Desktop/logfiles/" # set this to the full path of where you want logfiles to live

# init the LED STICK because we'll use it to indicate load progress
ledstick = ledstick.QwiicLEDStick()
ledstick.begin()
step = 7
def led_next(step):
    color = step % 7
    if ledstick.is_connected():
        if color == 0:
            ledstick.set_all_LED_color(209, 0, 0)
        elif color == 1:
            ledstick.set_all_LED_color(255, 102, 34)
        elif color == 2:
            ledstick.set_all_LED_color(255, 218, 33)
        elif color == 3:
            ledstick.set_all_LED_color(51, 221, 0)
        elif color == 4:
            ledstick.set_all_LED_color(17, 51,204)
        elif color == 5:
            ledstick.set_all_LED_color(34, 0, 102)
        elif color == 6:
            ledstick.set_all_LED_color(51, 0, 68)
        else:
            ledstick.set_all_LED_color(255, 255, 255)
    else:
        print("LED Stick not connected")
    return

led_next(step)
step += 1
# it is possible to pass in some arguments when you invoke the script
# -d [True|False] will override the debug flag to whichever boolean you supply
# -f [filename] will override the default file naming convention
# -g [True|False] will override the logging flag. Can be overridden if the script is unable to initialize a log file.
# -l [location] will set location to the arg and grafana to True - turns on grafana logging
# -p [####.###] Supply an override air pressure expressed as a decimal. 
#   Supplying more than 6.3 digits won't break this script, but it will be rejected by my Grafana database table for this data.

try:
    opts, args = getopt.getopt(sys.argv[1:], "d:f:g:l:p:")
    
except:
    print("Unable to get args. Will continue with script defaults.")
finally:
    for opt, arg in opts:
        if opt in ['-d']:
            debug = bool(arg)
        if opt in ['-f']:
            filename = arg.strip(" ")
        if opt in ['-g']:
            logging = bool(arg) #script is set to log if able to initialize a new file. This will turn the flag back off
        if opt in ['-l']:
            location = arg.strip(" ") # setting a location will enable logging for Grafana
            grafana = True
        if opt in ['-p']:
            pressure = arg # this can still be overridden by an attached pressure sensor
    if debug == True:
        print("Done getting args")
print("Setting some needed variables")

led_next(step)
step += 1

# set desired CO2 ranges. The "yellow" range will be the values between the HIGH and the LOW
CO2_HIGH = 1200 # the lowest PPM of the "high" range
CO2_LOW = 800 # the highest PPM of the "low" range

# customize text [and future LED] colors
green = (3, 252, 44) # the color to use for low ("good") levels
yellow = (240, 252, 3) # the color to use for medium ("almost bad") levels
red = (252, 3, 3) # the color to use for high ("bad") levels

# set a audio files for the audible alarm to use
yellow_alarm_f = '/home/pi/Apps/circuitpython/sounds/yellow.ogg' # this sound will loop while in yellow range
red_alarm_f = '/home/pi/Apps/circuitpython/sounds/red.ogg' # this sound will loop while in red range

# how long should we wait between sensor reads, in seconds
DATA_REFRESH = 5

# display stuff
text_color = green # initially sets the text and color to green
DISPLAY_HEIGHT = 480 # the screen's height in px
DISPLAY_WIDTH = 800 # the screen's width in px

print("setting up last resort variables")
# if you don't have an LPS22, you can set a number of degrees C
# to subtract from SCD30, which tends to run/read a few degrees hot
temperature_delta = 0 # positive int or float of degrees to subtract from SCD30
# if you won't have Internet access or a RTC, you can estmate your start time
rtc_time = time.gmtime(1630954461) #this takes 'seconds since epoch' for your date/time. 
time_delta = False # stores time.monotonic() for later delta calculation
date_time = False # initialize for time handling
t = False # initialize for time handling
if debug == True:
    print("Variables Set.. Initializing hardware")

led_next(step)
step += 1

# initialize hardware and display items
sensor = adafruit_scd30.SCD30(board.I2C())
pygame.init() # initialize pygame for screen drawing
pygame.mixer.init() # initialize pygame audio mixer for audible alarm
yellow_alarm = pygame.mixer.Sound(yellow_alarm_f) # loads the alarm audio file for playing
red_alarm = pygame.mixer.Sound(red_alarm_f)
pygame.font.init()
pygame.mouse.set_visible(False)
big_font = pygame.font.SysFont('nunito', 225)
ppm_font = pygame.font.SysFont('nunito', 85)
sparkline_font = pygame.font.SysFont('dejavusansmono', 55)
little_font = pygame.font.SysFont('nunito', 55)
screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN) # initialize the display
screen.fill(("black"))
pygame.display.update()


led_next(step)
step += 1

# touchscreen handling
ts = Touchscreen()
def touch_handler(event, touch):
    if event == TS_PRESS:
        global repeat
        repeat = False
    if event == TS_RELEASE:
        global released
        released = True
    if event == TS_MOVE:
        global moved
        moved = True

for touch in ts.touches:
    dir(touch)
    touch.on_press = touch_handler
    touch.on_release = touch_handler
    touch.on_move = touch_handler
ts.run()

if debug == True:
    print("Touchscreen handling enabled")

led_next(step)
step += 1

# timekeeping: try to get internet time, try RTC, or fail back to manually set time
# if a wireless connection is available, query the current time.
try:
    rtc_time = time.localtime()
    time_delta = time.monotonic()
except:
    print("I couldn't retrieve the current time from the Internet")
    # if RTC is available, create object and update pressure variables
    try:
        rtc = adafruit_pcf8523.PCF8523(board.I2C())
        rtc_time = rtc.datetime # expressed as struct_time
        time_delta = time.monotonic()
    except:
        time_delta = time.monotonic()
        print("I couldn't get the current time from a RTC")
finally:
    try:
        rtc = adafruit_pcf8523.PCF8523(board.I2C())
        rtc.datetime = time.localtime()
        if debug == True:
            print("Updated RTC with Internet Time")
    except:
        print("Could not update RTC with Internet Time")

led_next(step)
step += 1

if debug == True:
    print("Preparing for data logging")
# Prepare for SD card logging
# Get next incremental filename here and re-assign filename
# If this fails, logging will not occur, but display will continue
g_filename = False
i = 0 # initialize filename counter
try:
    if filename == True and logging == True:
        y = 0 # counter for traversing the file list
        sd_dir = os.listdir(logfile_path) # get the list of files
        sd_dir.sort() # sort the file list so the filenames are in order
        while y < len(sd_dir): #iterate over the file list looking for filename matches
            while sd_dir[y] == ("log%03d.csv" % i):
                i += 1 # we found a match, so increment the filename counter
            y += 1 # increment the list iterator to advance to the next filename
except:
        print("Could not read the next file number")
        logging = False
filename = logfile_path + ("log%03d.csv" % i) # create the filename from the counter
if grafana == True:
    g_filename = logfile_path + ("grafana%03d.csv" % i) # create filename for grafana log
try:
    with open(filename, "w+") as f:
        f.write("datetime, co2, temp, humidity, pressure\n")
    if debug == True:
        print(filename + " initialized")
except:
    print("Could not initialize regular log file.")
    logging = False
    print("Logging flag set to ", logging)
if grafana == True:
    try:
        with open(g_filename, "w+") as f:
            f.write("epoch_time, metric, location, reading \n")
        if debug == True:
            print(g_filename, " initialized")
    except:
        print("Could not initialize grafana log file")
        grafana = False
if debug == True:
    print("Done preparing for logging")
    print("Starting pressure sensor init")

led_next(step)
step += 1

# if LPS22 is available, create object and update the SCD30
# we'll also check for a temperature difference and set a delta
try:
    pressure_sen = adafruit_lps2x.LPS22(board.I2C())
    if debug == True:
        print("Pressure Sensor Debug (pressure: %f temp: %fC)" % (pressure_sen.pressure, pressure_sen.temperature))

except:
    sensor.ambient_pressure = pressure #average pressure at sea level; fallback default
    print("I couldn't connect to a pressure sensor, so I set the CO2 sensor's reference pressure to the default")
finally:
    print(sensor.temperature, type(sensor.temperature), pressure_sen.temperature, type(pressure_sen.temperature))
    sensor.ambient_pressure = pressure_sen.pressure # set SCD30 pressure variable to increase accuracy
    pressure = pressure_sen.pressure
    temperature_delta = sensor.temperature - pressure_sen.temperature # record difference in temperature for later
    if debug == True:
        print("The temperature delta is %f" % temperature_delta)
        print("Updated the CO2 sensor's pressure input")
if debug == True:
    print("Done with pressure sensor init")
    print("Now setting up display")

led_next(step)
step += 1

# set up screen text

# co2 label
co2_content = ' ' 
co2_label = big_font.render(co2_content, True, green)
co2_label_width = big_font.size(co2_content)
screen.blit(co2_label, (DISPLAY_WIDTH / 2 - co2_label_width[0] / 2, (DISPLAY_HEIGHT - co2_label_width[1])))

if debug == True:
    print("CO2 text label added!")

# PPM label
ppm = 'PPM'
ppm_label = ppm_font.render(ppm, True, green)
ppm_label_width = ppm_font.size(ppm)
screen.blit(ppm_label, (DISPLAY_WIDTH / 2 - ppm_label_width[0] / 2, (DISPLAY_HEIGHT - ppm_label_width[1]) * 0.6))

if debug == True:
    print("PPM text label added!")

# Temp/Humidity/Pressure label
other_content = '   °F |    % rH |       mbar'
other_label_width = little_font.size(other_content)
other_label = little_font.render(other_content, True, green)
screen.blit(other_label, (DISPLAY_WIDTH / 2 - other_label_width[0] / 2, (DISPLAY_HEIGHT - other_label_width[1]) * 0.98))

if debug == True:
    print("temp & rH label added!")

# add placeholder text to screen
pygame.display.update()

if debug == True:
    print("Pausing for a moment")
# if we don't have a short pause here, the sketch sometimes just stops
time.sleep(2)

repeat = True
time_last = int(time.monotonic()) - 5
pressure_last = int(time.monotonic())

# init sparkline list
co2_list = []
for x in range(25):
    co2_list.append(float(0))

if debug == True:
    print("Ready to enter the main loop")
print("Debug:", debug, "Logging:", logging, "Location:", location, "Pressure:", pressure)

led_next(step)

while True:
    #if debug == True:
        #print("Successfully entered the main loop...")

    # wait for the sensor to report that it has a reading, then store the readings into local variables
    try:
        if (int(time.monotonic()) - time_last) >= DATA_REFRESH:
            if sensor.data_available: 
                if debug == True:
                   print("Successfully met conditions for taking a reading.")
                time.sleep(1)    
                # convert the temperature reading to F, and trim to 1 decimal place
                temperature = round(((sensor.temperature - temperature_delta)* 9 / 5) + 32, 1)
                co2 = sensor.CO2 # store the CO2 reading
                co2_list.append(float(co2)) # add the CO2 Reading to the sparkline list
                if len(co2_list) > 25:
                    co2_list.pop(0) # trim the sparkline list by removing the oldest item
                humidity = round(sensor.relative_humidity, 1) # store the rH reading and trim to 1 decimal place
                try:
                    pressure = pressure_sen.pressure
                except:
                    exception_type, exception_object, exception_traceback = sys.exc_info()
                    error_file = exception_traceback.tb_frame.f_code.co_filename
                    line_number = exception_traceback.tb_lineno
                    print("I had a problem updating the pressure")
                    print("Exception Type: ", exception_type)
                    print("File name: ", error_file)
                    print("Line: ", line_number)
                else: 
                    if int(time.monotonic()) - pressure_last > 60:
                        sensor.pressure = pressure
                        pressure_last = int(time.monotonic())
                        print("====================================================")
                        print("Updated the CO2 Sensor with current pressure reading")
                        print("====================================================")
                    temperature = round((pressure_sen.temperature * 9 / 5) + 32, 1)
                    if debug == True:
                        print("Successfully updated pressure and temperature reading")
                # determine what color and sound file we should use based on CO2 concentration
                try:
                    if co2 > CO2_HIGH:
                        # text and LED color
                        text_color = red
                        led_color = yellow
                        # audible alarm sound file
                        if pygame.mixer.Sound.get_num_channels(red_alarm) < 1:
                            if pygame.mixer.Sound.get_num_channels(yellow_alarm) > 0:
                                pygame.mixer.Sound.fadeout(yellow_alarm, 1000)
                            pygame.mixer.Sound.play(red_alarm, loops=-1, fade_ms=1000)
                    elif co2 > CO2_LOW:
                        # text and LED color
                        text_color = yellow
                        led_color = yellow
                        # audible alarm sound file
                        if pygame.mixer.Sound.get_num_channels(yellow_alarm) < 1:
                            if pygame.mixer.Sound.get_num_channels(red_alarm) > 0:
                                pygame.mixer.Sound.fadeout(red_alarm, 1000)
                            pygame.mixer.Sound.play(yellow_alarm, loops=-1, fade_ms=1000)
                    else:
                        # text and LED color
                        text_color = green
                        led_color = green
                        ledstick.set_all_LED_color(green[0], green[1], green[2])
                        # audible alarm sound file (we're turning off all sound at this level
                        if pygame.mixer.Sound.get_num_channels(yellow_alarm) > 0:
                            pygame.mixer.Sound.fadeout(yellow_alarm, 1000)
                        if pygame.mixer.Sound.get_num_channels(red_alarm) > 0:
                            pygame.mixer.Sound.fadeout(red_alarm, 1000)
                    try:
                        if ledstick.is_connected():
                            ledstick.set_all_LED_color(led_color[0], led_color[1], led_color[2])
                    except:
                        exception_type, exception_object, exception_traceback = sys.exc_info()
                        error_file = exception_traceback.tb_frame.f_code.co_filename
                        line_number = exception_traceback.tb_lineno
                        print("I couldn't update the LED color")
                        print("Exception: ", exception_type)
                        print("File: ", error_file)
                        print("Line: ", line_number)
                except:
                    exception_type, exception_object, exception_traceback = sys.exc_info()
                    error_file = exception_traceback.tb_frame.f_code.co_filename
                    line_number = exception_traceback.tb_lineno
                    print("Error setting audio!")
                    print("Exception: ", exception_type)
                    print("File: ", error_file)
                    print("Line: ", line_number)
                    
                # redraw the screen
                try:
                    screen.fill(pygame.Color("black"))

                    # co2 label
                    co2_text = str(int(co2)) # update the co2_text label with the fresh data
                    co2_label_width = big_font.size(co2_text)
                    co2_label = big_font.render(co2_text, True, text_color)
                    screen.blit(co2_label, (DISPLAY_WIDTH / 2 - co2_label_width[0] / 2, (DISPLAY_HEIGHT - co2_label_width[1]) * 0.001))

                    # PPM Label
                    ppm_label = ppm_font.render(ppm, True, text_color)
                    ppm_label_width = ppm_font.size(ppm)
                    screen.blit(ppm_label, (DISPLAY_WIDTH / 2 - ppm_label_width[0] / 2, (DISPLAY_HEIGHT - ppm_label_width[1]) * 0.60))

                    # sparkline label
                    sparkline_list = sparklines.sparklines(co2_list) # sparkify our list of CO2 readings
                    sparkline_text = " ".join([str(item) for item in sparkline_list]) # convert the list to a string
                    sparkline_label = sparkline_font.render(sparkline_text, True, text_color) # render the sparkline
                    screen.blit(sparkline_label, ((DISPLAY_WIDTH * 0.01), (DISPLAY_HEIGHT * 0.65))) #add the label to the screen

                    # temp/humidity/pressure
                    other_text = '%0.1f°F | %0.1f%%rH | %0.1f mbar' % (temperature, humidity, pressure) # update the temp/rH label
                    other_label = little_font.render(other_text, True, text_color)
                    other_text_width = little_font.size(other_text)
                    screen.blit(other_label, (DISPLAY_WIDTH / 2 - other_text_width[0] / 2, (DISPLAY_HEIGHT - other_text_width[1]) * 0.98))

                    # push all updated labels to the screen
                    pygame.display.update()

                except:
                    exception_type, exception_object, exception_traceback = sys.exc_info()
                    error_file = exception_traceback.tb_frame.f_code.co_filename
                    line_number = exception_traceback.tb_lineno
                    print("Problem encountered while Updating the Screen!")
                    print("Exception: ", exception_type)
                    print("File: ", error_file)
                    print("Line: ", line_number)

                # update current time for logfiles
                if rtc_time != False:
                    try:
                        current = (time.monotonic())
                        seconds = (current - time_delta)
                        date_time = (time.mktime(rtc_time) + int(seconds))
                        t = time.localtime(date_time)
                    except:
                        exception_type, exception_object, exception_traceback = sys.exc_info()
                        error_file = exception_traceback.tb_frame.f_code.co_filename
                        line_number = exception_traceback.tb_lineno
                        print("Nope!")
                        print("Exception: ", exception_type)
                        print("File: ", error_file)
                        print("Line: ", line_number)
                else:
                    date_time = time.monotonic()

                if pressure_sen:
                    pressure = pressure_sen.pressure
                
                if logging == True:
                    try:
                        datestring = '%04d-%02d-%02d_%02d:%02d:%02d' % (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec)
                        with open(filename, "a") as f:
                            f.write(str(datestring) + "," + str(co2) + "," + str(temperature) + "," + str(humidity) + "," + str(pressure) +"\n")
                            if debug == True:
                                print(datestring, " ", co2, " ", temperature, " ", humidity, " ", pressure, " written to log")
                        if grafana == True: # epoch_time, metric, location, reading
                            epoch_time = time.mktime(time.localtime())
                            with open(g_filename, "a") as f:
                                f.write(str(int(epoch_time)) + ",co2," + location + "," + str(round(co2, 3)) + "\n")
                                f.write(str(int(epoch_time)) + ",temperature," + location + "," + str(round(temperature, 3)) + "\n")
                                f.write(str(int(epoch_time)) + ",humidity," + location + "," + str(round(humidity, 3)) + "\n")
                                f.write(str(int(epoch_time)) + ",pressure," + location + "," + str(round(pressure, 3)) + "\n")
                                if debug == True:
                                    print(int(epoch_time), " ", location, " ", round(co2, 3), " ", round(temperature, 3), " ", round(humidity, 3), " ", round(pressure, 3), " written to Grafana log")
                    except:
                        exception_type, exception_object, exception_traceback = sys.exc_info()
                        error_file = exception_traceback.tb_frame.f_code.co_filename
                        line_number = exception_traceback.tb_lineno
                        print("Logging Failed")
                        print("Exception: ", exception_type)
                        print("File: ", error_file)
                        print("Line: ", line_number)

    
            time_last = int(time.monotonic()) # update time we last polled the CO2 Sensor
            
    except Exception:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        error_file = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        print("Encountered a problem while trying to poll the sensors.")
        print("If the exception is 'OS Error', check to see if your sensors are connected.")
        print("Exception: ", exception_type)
        print("File: ", error_file)
        print("Line: ", line_number)

    if repeat == False:
        # we're going to exit the script
        ledstick.LED_off() #let's turn the LEDs off
        os._exit(1) # terminate current thread
        break
    else:
        continue
    os._exit(1)
    break
ts.stop() # stop the screen
sys.exit() # exit the script

