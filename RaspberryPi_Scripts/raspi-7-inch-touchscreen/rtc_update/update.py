import os
import adafruit_pcf8523
import qwiic_led_stick as ledstick
import time

try:
    ledstick = ledstick.QwiicLEDStick()
except:
    pass

try:
    rtc = adafruit_pcf8523.PCF8523(board.I2C())
    date_str = rtc.datetime

    os.system('date -s %s' % date_str)
except:
    import sys
    sys.exit()
finally:
    print("Time update successful")
    for i in range (0, 5, 1):
        try:
            ledstick.set_all_LED_color(3, 252, 44)
            time.sleep(1)
            ledstick.set_all_LED_color(0, 0, 0)
            time.sleep(1)
        except:
            pass
