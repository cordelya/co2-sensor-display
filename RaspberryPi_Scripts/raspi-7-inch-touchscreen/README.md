# CO2 Monitor / Logger for Raspberry Pi with Official 7" Touchscreen #

This script is specifically for use on a Raspberry Pi paired with an official RasPi 7" touchscreen. It should work on any RasPi that you can connect Stemma sensors to via the I2C. It has been tested with the following hardware: (items labeled _Coming Soon_ aren't supported in this script yet but will be soon - if you already have the hardware, you can always modify the script yourself)

1. Raspberry Pi 3
2. Raspberry Pi official 7" Touchscreen (purchased 2017)
3. Stemma SCD-30 (CO2 Sensor) breakout board
4. Stemma LPS22 (pressure, temperature) breakout board
5. Stemma PCF8523 (real-time clock) breakout board
6. SparkFun Qwiic / STEMMA QT HAT for Raspberry Pi
7. Enough Qwiic / STEMMA QT connector cables to connect the sensors to the hat
8. _Coming Soon:_ support for the Pimoroni LED SHIM for Raspberry Pi

This script has the following non-standard prerequisites:

  * CircuitPython Blinka library (available via pip as Adafruit-Blinka)
  * adafruit_scd30 (available via pip)
  * adafruit_lps2x (available via pip)
  * adafruit_pcf8523 (available via pip)
  * python3-pygame (available via apt)
  * ft5406 (https://github.com/pimoroni/python-multitouch) -see note about this below
  * _Coming soon:_ Pimoroni LED SHIM Library (https://github.com/pimoroni/led-shim)

Note about ft5406: the library as available in the GitHub repository needs a modification before you install it. In /library/ft5406.py, on line 115:

`TOUCHSCREEN_EVDEV_NAME = 'FT5406 memory based driver'`

This line should be changed to read:

`TOUCHSCREEN_EVDEV_NAME = 'raspberrypi-ts'`

Once you have saved that file, you may then follow the instructions on the repository's README to install the library.

## Audible Alarms

This script now plays audio files as audible alarms any time the co2 reading is in the yellow or red range. If you do not want to use this feature, you will need to comment out all lines of code that include `pygame.mixer.[...]` or as an alternative, you can drop two random (but valid and playable) audio files in your directory tree, edit the script to point to them, and either set the system volume to zero or never connect any audio equipment.

A note regarding the Pimoroni LED SHIM vs the Qwiic / STEMMA QT HAT: the LED SHIM and full-width QT HAT probably won't play nice together without some additional hardware to split the GPIO access. I'm planning to switch to a QT SHIM, which is only just large enough to accommodate the necessary GPIO pins and one quick-connect receptacle. I will probably still need at least one pass-through header to give them enough space to coexist. Also coming is a version intended to be used with a Pi Zero W (with headers) and a 2.8" TFT that uses (a different portion of) the GPIO header. That different display will require different Python libraries, hence the (coming) separate script.
