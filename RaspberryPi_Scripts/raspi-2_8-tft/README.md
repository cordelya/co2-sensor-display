# For Pi with Adafruit PiTFT

-----

:rotating_light: In early April 2022, the machine I had running this script received an update that broke `pygame`'s ability to access the framebuffer. As of this document's most recent update, I still have not been able to return the machine to a state where `pygame` works properly. See the version that uses `pySimpleGui` for a workaround :rotating_light:

-----

This was tested using a Raspberry Pi Zero W and a PiTFT 2.8" display with support for Raspberry Pi models with 2x13 GPIO. It will likely work on other PiTFT models because the libraries are similar - follow the setup guide for the model of PiTFT you choose.

Other hardware: 
- Adafruit SCD-30 CO2 Sensor
- Adafruit PCF8523 Real-time clock (optional)
- Adafruit LPS22 Pressure Sensor (optional)
- Pimoroni LED SHIM
- Sparkfun Qwiic adapter
  - Note: I intended to use the Sparkfun Qwiic SHIM as the Qwiic HAT blocks the LED SHIM, and requires some kind of GPIO duplicator so that the PiTFT and the Qwiic HAT can connect to the GPIO (neither one has a pass-through accessible to other GPIO devices without extra hardware). Unfortunately, at the time this README was written, neither of the two Qwiic SHIMs I received have worked properly (too loose, most likely).
- If an LPS22 Pressure Sensor is attached, the script will use readings from it to make the CO2 readings more accurate. This sensor is optional, and the script has a default pressure reading to use, or you may supply one at launch. Appending `-p 1008` to the end of your command when launching the script will make the script pass a pressure of 1008 to the CO2 sensor.
- The script will gracefully fall back to other methods of time-updating and timekeeping. A PCF8523 RTC is optional.

Using this with any display other than an Adafruit PiTFT is pretty much guaranteed to not work out of the box. You will most likely need to make some adjustments.

Flash your SD card with the Desktop version of Raspbian.

The required Python libraries are listed in requirements.txt and can be installed all at once by invoking

```
pip3 install -r requirements.txt
```

from the folder where requirements.txt resides (or change the file path to point to it if invoking from elsewhere). NOTE: currently requirements.txt contains the list for using pygame. If you're planning to use the pySimpleGUI variation, you can still use requirements.txt but you will also need to do `python3 -m pip install pysimplegui` in order to make it work correctly.

For the `pigame` module you will need to follow the [install instructions](https://pigamedrv.github.io/userdoc/install)

This script uses the #18 button to suspend the script, as I could not get pygame event handling and touchscreen touch events to play nicely. You'll either need to install a button in the #18 spot or plan to use a jumper to complete the circuit when you want to safely suspend the script!

You can play the audible alarms with a Pi Zero by pairing/connecting the board's Bluetooth module to a BT speaker of your choice.
- You must provide your own audible alarm files - save them as `/home/pi/Music/red.ogg` and `/home/pi/Music/yellow.ogg` or edit the script to reflect your chosen filenames & paths

You can make it easy to launch the script from the touchscreen by creating a bash script in the "Desktop" folder and making it executable. When you double-tap the executable script, it will ask how you want to run it. You want to choose "Run in Terminal".

## pySimpleGUI variation

April 2022 brought a variation that uses the `pySimpleGUI` python library. The output on a 2.8" PiTFT will be nearly identical to that of the variation that uses `pygame`.

The `pySimpleGUI` variation generates a borderless maximized window on the desktop and displays the information there in a layout that is nearly identical to the `pygame` variation layout. 

This variation relies on font size to make the text fit. Each row is a separate text element. There is an empty row at the top with `expand_x` and `expand_y` set to `True` to push the rest of the rows down so they're bottom-aligned.
