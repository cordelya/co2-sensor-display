import os
import adafruit_pcf8523
import import ledshim
import time

try:
    ledshim.set_clear_on_exit()
except:
    pass

try:
    rtc = adafruit_pcf8523.PCF8523(board.I2C())
    date_str = rtc.datetime

    os.system('date -s %s' % date_str)
except:
    import sys
    sys.exit()
finally:
    print("Time update successful")
    for i in range (0, 5, 1):
        try:
            ledshim.set_all(3, 252, 44)
            time.sleep(1)
            ledshim.set_all(0, 0, 0)
            time.sleep(1)
        except:
            pass
