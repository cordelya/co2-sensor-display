# Sensor Logging and Display Script 
# by Cordelya Sharpe
# based on, in part, several examples at https://learn.adafruit.com
# Raspberry Pi + Official 7" Touchscreen Edition
# Licensed under the MIT License. See https://gitlab.com/cordelya/co2-sensor-display

import time
import datetime
import board
import busio
import adafruit_scd30
import adafruit_lps2x
import adafruit_pcf8523
import os
import sys
import getopt
import ledshim
import PySimpleGUI as sg
import sparklines
import RPi.GPIO as GPIO
import colorama
from colorama import Fore, Back, Style

# initialize colorama
colorama.init(autoreset=True)

# ============================ Functions ===================================== #

# ---------------------------------------------------------------------------- #
#       FUNCTION: Button Detection                                             #
# ---------------------------------------------------------------------------- #
# This function detects button presses on the 4 hardware buttons that come     #
# with the Adafruit 2.8" Resistive PiTFT screen. Possible GPIO pin numbers are:#
# Left to right: 18, 27, 22, and 23 (if located above screen)                  #
# Right to left: 23, 22, 27, and 18 (if located below screen)                  #
# ---------------------------------------------------------------------------- #

def btn_detect():
    try:
        for (k,v) in button_map.items():
            if GPIO.input(k) == False and v == 18:
                print("Exit Button Pressed!")
                global repeat
                repeat = False
            if GPIO.input(k) == False and v == 27:
                print("Button 21 pressed. This button doesn't do anything.")
            if GPIO.input(k) == False and v == 22: 
                print("Button 22 pressed. This button doesn't do anything.")
            if GPIO.input(k) == False and v == 23: 
                print("Button 23 pressed. This button doesn't do anything.")
    except:
        print("Button Press Detection Failed")

# ---------------------------------------------------------------------------- #
#      FUNCTION: Color Step                                                    #
# ---------------------------------------------------------------------------- #
# This function will change the color displayed on the LED Shim based on the   #
# current value of `step`. The purpose is to provide a visual indication of    #
# progress in initializing everything prior to the main loop.                  #
# You can add colors to the progression by inserting another elif above else   #
# and increasing the value of `color %= 7` by however many elif statements you #
# add.                                                                         #
# ---------------------------------------------------------------------------- #

def led_next(color):
    global debug
    if debug == True:
        print("Step ", color)
    if color == 0:
        ledshim.set_all(209, 0, 0)
        ledshim.show()
    elif color == 1:
        ledshim.set_all(255, 102, 34)
        ledshim.show()
    elif color == 2:
        ledshim.set_all(255, 218, 33)
        ledshim.show()
    elif color == 3:
        ledshim.set_all(51, 221, 0)
        ledshim.show()
    elif color == 4:
        ledshim.set_all(17, 51, 204)
        ledshim.show()    
    elif color == 5:
        ledshim.set_all(34, 0, 102)
        ledshim.show()    
    elif color == 6:
        ledshim.set_all(51, 0, 68)
        ledshim.show() 
    else:
        ledshim.set_all(255, 255, 255)
        ledshim.show()
    color += 1
    color %= 7
    global step
    step = color
    return

# ----------------------------------------------------------------------------#
#      FUNCTION: get_opts()                                                   #
# ----------------------------------------------------------------------------#
# it is possible to pass in some arguments when you invoke the script:        #
#   -d [True|False] will override the debug flag to whichever bool you supply #
#   -f [filename] will override the default file naming convention            #
#   -g [True|False] will override the logging flag. Can be overridden if the  #
#      script is unable to initialize a log file.                             #
#   -l [location] will set location to the arg and grafana to True - turns on #
#      grafana logging                                                        #
#   -p [####.###] Supply an override air pressure expressed as a decimal.     #
#      Supplying more than 6.3 digits won't break this script, but it will be #
#      rejected by my Grafana database table for this data.                   #
# ----------------------------------------------------------------------------#

def get_opts(opts, extras):
    global debug, filename, logging, location, grafana, pressure
    if debug:
        print("Attempting to pull args")
    try:
        for opt, arg in opts:
            if opt in ['-d']:
                debug = bool(arg)
                if debug == True:
                    print("Debug flag = ", debug)
            if opt in ['-f']:
                filename = arg.strip(" ")
                if debug == True:
                    print("Filename set to", filename)
            if opt in ['-g']:
                logging = bool(arg)
                if debug == True:
                    print("Logging turned OFF")
                    print("Logging flag =", logging)
            if opt in ['-l']:
                location = arg.strip(' ')
                grafana = True
                if debug == True:
                    print("Location set to ", location)
                    print("Grafana logging flag set to ", grafana)
            if opt in ['-p']:
                pressure = int(arg)
                if debug == True:
                    print("Pressure set to ", pressure) # this can still be overridden by an attached pressure sensor
        if debug == True:
            print("Done getting args")
            print("Debug: ", debug, ", filename: ", filename, ", logging: ", logging, ", location: ", location, ", grafana: ", grafana, ", pressure: ", pressure)
    except:
            print('Unable to obtain values from opts')
    return

def init_file():
    global debug, filename, logging, grafana, g_filename, logfile_path
    i = 0 # initialize filename counter
    try: 
        if filename == True and logging == True:
            y = 0 # counter for traversing the file list
            sd_dir = os.listdir(logfile_path) # get the list of files
            if debug:
                print("File List")
                print(sd_dir)
            sd_dir.sort() # sort the file list so the filenames are in order
            while y < len(sd_dir): # iterate over the file list looking for matches
                while sd_dir[y] == ('log%03d.csv' % i):
                    if debug:
                        print("Found a match at location ", i)
                    i += 1 # we found a match, so increment the filename counter
                y += 1 # increment the list iterator to advance to the next filename
    except:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        error_file = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        print("Hmmm... that didn't work as expected.")
        print("Exception: ", exception_type)
        print("File: ", error_file)
        print("Line: ", line_number)
        print("Could not read the next file number")
        logging = False
    filename = logfile_path + ("log%03d.csv" % i) # create the filename from the counter
    if grafana == True:
        g_filename = logfile_path + ("grafana%03d.csv" % i) # create filename for grafana log
    return

def poll_scd30():
    global co2, temperature, sensor
    while sensor.data_available:
        try:            
            temperature = round(sensor.temperature * (9 / 5) + 32, 1)
            co2 = int(sensor.CO2)
        except:
            print('Could not read from the CO2 Sensor')
    return

def poll_pressure():
    global pressure, pressure_sen
    try: 
        pressure = pressure_sen.pressure
    except: 
        print('Could not read pressure')
    return

def update_pressure():
    global pressure, sensor
    try:
        sensor.pressure = pressure
    except: 
        print('Could not update the CO2 Sensor with pressure reading')
    return

def update_led(color): # the value of 'color' should be comma-separated RGB values
    try:
            ledshim.set_all(color)
            ledshim.show()
    except:
        print('Could not update the LED SHIM')
    return

def update_sound(color): # pass in green, yellow, or red indicator and update audio
    pass

def update_text(color): # pass in green, yellow, or red indicator and update text
    pass

def update_time():
    global rtc_time, current, seconds, date_time, t
    try:
        if rtc_time != False:
            current = (time.monotonic())
            seconds = (current - time_delta)
            date_time = (time.mktime(rtc_time) + int(seconds))
            t = time.localtime(date_time)
        else:
            date_time = time.monotonic()
    except:
        print('Could not update timekeeping')
    return

def write_log():
    global debug, t, pressure, filename, g_filename, grafana, location, humidity, temperature, co2
    datestring = '%04d-%02d-%02d_%02d:%02d:%02d' % (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec)
    with open(filename, "a") as f:
        f.write(str(datestring) + "," + str(co2) + "," + str(temperature) + "," + str(humidity) + "," + str(pressure) +"\n")
        if debug == True:
            print(datestring, " ", co2, " ", temperature, " ", humidity, " ", pressure, " written to log")
        if grafana == True: # epoch_time, metric, location, reading
            epoch_time = time.mktime(time.localtime())
            with open(g_filename, "a") as f:
                f.write(str(int(epoch_time)) + ",co2," + location + "," + str(round(co2, 3)) + "\n")
                f.write(str(int(epoch_time)) + ",temperature," + location + "," + str(round(temperature, 3)) + "\n")
                f.write(str(int(epoch_time)) + ",humidity," + location + "," + str(round(humidity, 3)) + "\n")
                f.write(str(int(epoch_time)) + ",pressure," + location + "," + str(round(pressure, 3)) + "\n")
                if debug == True:
                    print(int(epoch_time), " ", location, " ", round(co2, 3), " ", round(temperature, 3), " ", round(humidity, 3), " ", round(pressure, 3), " written to Grafana log")


# ========= Variables & Init ========== #

debug = False # to turn on debugging, set this to true, or pass -d True when invoking this script
logging = True # to prevent the script from logging readings, pass `-g False` when invoking this script
location = 'unknown' # to change this pass `-l [location]` when invoking the script. Used for Grafana logging
pressure = 1013.25 # default pressure to use when there is no pressure sensor. Pass `-p [pressure]` to manually change
filename = True # this will hold the filename for logging
grafana = False # False by default. To turn on, pass a location using `-l [location]` when invoking this script
g_filename = False # if Grafana logging is enabled, this will be changed to hold the filename
logfile_path = "/home/pi/logs/" # set this to the full path of where you want logfiles to live

# set desired CO2 ranges. The "yellow" range will be the values between the HIGH and the LOW
CO2_HIGH = 1200 # the lowest PPM of the "high" range
CO2_LOW = 800 # the highest PPM of the "low" range

# customize text [and future LED] colors
green = (3, 252, 44) # the color to use for low ("good") levels
yellow = (240, 252, 3) # the color to use for medium ("almost bad") levels
red = (252, 3, 3) # the color to use for high ("bad") levels

# set a audio files for the audible alarm to use
yellow_alarm_f = '/home/pi/Music/yellow.ogg' # this sound will loop while in yellow range
red_alarm_f = '/home/pi/Music/red.ogg' # this sound will loop while in red range

# how long should we wait between sensor reads, in seconds
DATA_REFRESH = 5

# display stuff
text_color = green # initially sets the text and color to green
DISPLAY_HEIGHT = 480 # the screen's height in px
DISPLAY_WIDTH = 640 # the screen's width in px

# if you won't have Internet access or a RTC, you can estmate your start time
rtc_time = time.gmtime(1630954461) #this takes 'seconds since epoch' for your date/time. 
time_delta = False # stores time.monotonic() for later delta calculation
date_time = False # initialize for time handling
t = False # initialize for time handling

repeat = True # initialize the repeat flag for the main loop

# init the LED SHIM because we'll use it to indicate load progress
ledshim.set_clear_on_exit()
step = 7
led_next(step)

# fetch any args that were passed
try:
    opts, extras = getopt.getopt(sys.argv[1:], "d:f:g:l:p:")
    get_opts(opts, extras)
except:
    print("Could not obtain args from script invocation")

# init the LPS22
try:
    pressure_sen = adafruit_lps2x.LPS22(board.I2C())
    pressure = pressure_sen.pressure
except:
    print("Could not connect to the LPS22. Falling back to default.")

led_next(step)

if debug:
    print("Pressure: ", pressure)

# init the scd30
try:
    sensor = adafruit_scd30.SCD30(board.I2C())
except:
    print("Could not establish CO2 Sensor Object. Exiting.")
    time.sleep(5)
    ledshim.fill(0,0,0)
    ledshim.show()
    os._exit(1)
try:
    sensor.ambient_pressure = pressure
except:
    print("Could not update the CO2 Sensor with current pressure reading")

led_next(step)

# init the exit button
# GPIO 18 will be used for the exit button. Remaining 3 buttons remain unprogrammed, available for other uses
button_map = {23:23, 22:22, 27:27, 18:18}
GPIO.setmode(GPIO.BCM)
for k in button_map.keys():
    GPIO.setup(k, GPIO.IN, pull_up_down=GPIO.PUD_UP)

led_next(step)

# initialize audio and define alarm files
#pygame.mixer.init() # initialize pygame audio mixer for audible alarm
#yellow_alarm = pygame.mixer.Sound(yellow_alarm_f) # loads the alarm audio file for playing
#red_alarm = pygame.mixer.Sound(red_alarm_f)

led_next(step)

# set font sizes for our lines
#pygame.font.init()
#pygame.mouse.set_visible(False)
#big_font = pygame.font.SysFont('nunito', 216)
#ppm_font = pygame.font.SysFont('nunito', 72)
#sparkline_font = pygame.font.SysFont('dejavusansmono', 60)
#little_font = pygame.font.SysFont('nunito', 42)

led_next(step)

#ts = PiTft()
#screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN) # initialize the display
#screen.fill((0, 0, 0))
#pygame.display.update()
#pygame.fastevent.init()


led_next(step)

# timekeeping: try to get internet time, try RTC, or fail back to manually set time
# if a wireless connection is available, query the current time.
try:
    rtc_time = time.localtime()
    time_delta = time.monotonic()
except:
    print("I couldn't retrieve the current time from the Internet")
    # if RTC is available, create object and update pressure variables
    try:
        rtc = adafruit_pcf8523.PCF8523(board.I2C())
        rtc_time = rtc.datetime # expressed as struct_time
        time_delta = time.monotonic()
    except:
        time_delta = time.monotonic()
        print("I couldn't get the current time from a RTC")
finally:
    try:
        rtc = adafruit_pcf8523.PCF8523(board.I2C())
        rtc.datetime = time.localtime()
        if debug == True:
            print("Updated RTC with Internet Time")
    except:
        print("Could not update RTC with Internet Time")

led_next(step)

if debug == True:
    print("Step:", step)
    print("Preparing for data logging")

# Prepare for data logging
init_file()

led_next(step)

try:
    with open(filename, "w+") as f:
        f.write("datetime, co2, temp, humidity, pressure\n")
    if debug == True:
        print(filename + " initialized")
except:
    print("Could not initialize regular log file.")
    logging = False
    print("Logging flag set to ", logging)
if grafana == True:
    try:
        with open(g_filename, "w+") as f:
            f.write("epoch_time, metric, location, reading \n")
        if debug == True:
            print(g_filename, " initialized")
    except:
        print("Could not initialize grafana log file")
        grafana = False

led_next(step)

if debug == True:
    print("Step:", step)
    print("Done preparing for logging")
    print("Starting pressure sensor init")

# if LPS22 is available, create object and update the SCD30
# we'll also check for a temperature difference and set a delta
try:
    pressure_sen = adafruit_lps2x.LPS22(board.I2C())
    if debug == True:
        print("Pressure Sensor Debug (pressure: %f temp: %fC)" % (pressure_sen.pressure, pressure_sen.temperature))
    sensor.ambient_pressure = pressure_sen.pressure
    pressure = pressure_sen.pressure
    temperature_delta = sensor.temperature - pressure_sen.temperature # record difference in temperature for later
except:
    sensor.ambient_pressure = pressure #average pressure at sea level; fallback default
    print("I couldn't connect to a pressure sensor, so I set the CO2 sensor's reference pressure to the default")

    if debug == True:
        print("Updated the CO2 sensor's pressure input")

led_next(step)

if debug == True:
    print("Done with pressure sensor init")
    print("Now setting up graphical presentation")

# check for DISPLAY env var and set it if unset
if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using :0.0')
    os.environ.__setitem__('DISPLAY', ':0.0')

# PySimpleGUI
layout = [  [sg.Text('', background_color = '#000000', expand_x = True, expand_y = True)],
            [sg.Text('CO2', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'co2', font = ('Nunito', 144, 'bold'))],
            [sg.Text('PPM', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'ppm', font = ('Nunito', 36))],
            [sg.Text('Sparkline', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'sparkline', font = ('Nunito', 36))],
            [sg.Text('Temp, pressure, humidity', text_color = '#03FC2C', background_color = '#000000', justification = 'center', key = 'other', font = ('Nunito', 32))]  ]
window = sg.Window('CO2 Monitor', layout, background_color = '#000000', finalize = True, resizable=True, element_justification = 'center')
window.Maximize()


led_next(step)

if debug == True:
    print("Step:", step)
    print("Pausing for a moment")
# if we don't have a short pause here, the sketch sometimes just stops
time.sleep(2)

# init variables for the main loop
repeat = True
time_last = int(time.monotonic()) - 5
pressure_last = int(time.monotonic())
if debug == True:
    print("Ready to enter the main loop")

# init sparkline list
co2_list = []
for x in range(18):
    co2_list.append(float(800))

while True:
    
    # check for button presses
    btn_detect() 

    # wait for the sensor to report that it has a reading, then store the readings into local variables
    try:
        if (int(time.monotonic()) - time_last) >= DATA_REFRESH:
            if sensor.data_available:
                if debug == True:
                    print("Successfully met conditions for taking a reading.")
                    
                # convert the temperature reading to F, and trim to 1 decimal place
                temperature = round(((sensor.temperature)* 9 / 5) + 32, 1)
                co2 = sensor.CO2 # store the CO2 reading
                co2_list.append(float(co2)) # add the CO2 Reading to the sparkline list
                if len(co2_list) > 18:
                    co2_list.pop(0) # trim the sparkline list by removing the oldest item
                humidity = round(sensor.relative_humidity, 1) # store the rH reading and trim to 1 decimal place
                try: 
                    if pressure_sen:
                        pressure = pressure_sen.pressure
                        if int(time_monotonic() - pressure_last) > 60:
                            sensor.pressure = pressure # if the last time the pressure was updated was longer than 60 seconds, update it
                        temperature = round((pressure_sen.temperature * 9 / 5) + 32, 1) # pressure sensor's temp reading is more accurate
                except:
                    if debug == True:
                        print("A pressure sensor is not connected")
                
                # generate a string for the terminal printer
                prt_str = str(round(co2)) + ' PPM | ' + str(temperature) + '\u00b0F | ' + str(humidity) + '%rH'
                  

                # determine what color and sound-file we should use based on CO2 concentration
                if co2 > CO2_HIGH:
                    # text color and LED
                    t_color = '#FC0303'
                    ledshim.set_all(red[0], red[1], red[2])
                    ledshim.show()
                                        
                    # display information with color-coding via colorama
                    print(Fore.RED + prt_str)
                    
                    # audible alarm sound file
                    #if pygame.mixer.Sound.get_num_channels(red_alarm) < 1:
                    #    if pygame.mixer.Sound.get_num_channels(yellow_alarm) > 0:
                    #        pygame.mixer.Sound.fadeout(yellow_alarm, 1000)
                    #    pygame.mixer.Sound.play(red_alarm, loops=-1, fade_ms=1000)
                elif co2 > CO2_LOW:
                    # text color and LED
                    t_color = '#F0FC03'
                    ledshim.set_all(yellow[0], yellow[1], yellow[2])
                    ledshim.show()
                    
                    #display information with color-coding via colorama
                    print(Fore.YELLOW + prt_str)

                    # audible alarm sound file
                    #if pygame.mixer.Sound.get_num_channels(yellow_alarm) < 1:
                    #    if pygame.mixer.Sound.get_num_channels(red_alarm) > 0:
                    #        pygame.mixer.Sound.fadeout(red_alarm, 1000)
                    #    pygame.mixer.Sound.play(yellow_alarm, loops=-1, fade_ms=1000)
                else:
                    # text color and LED
                    t_color = '#03FC2C'
                    ledshim.set_all(green[0], green[1], green[2])
                    ledshim.show()
                    
                    # display information with color-coding via colorama
                    print(Fore.GREEN + prt_str)
                    
                    # audible alarm sound file
                    #if pygame.mixer.Sound.get_num_channels(yellow_alarm) > 0:
                    #    pygame.mixer.Sound.fadeout(yellow_alarm, 1000)
                    #if pygame.mixer.Sound.get_num_channels(red_alarm) > 0:
                    #    pygame.mixer.Sound.fadeout(red_alarm, 1000)
                
                # update the screen

                # co2 label
                co2_text = str(int(co2)) # update the co2_text label with the fresh data
                #co2_label_width = big_font.size(co2_text)
                #co2_label = big_font.render(co2_text, True, text_color)
                
                # PPM label
                #ppm_label = ppm_font.render(ppm, True, text_color)
                #ppm_label_width = ppm_font.size(ppm)

                # sparkline with the most recent co2 readings
                sparkline_list = sparklines.sparklines(co2_list) #produces a list
                sparkline_text = " ".join([str(item) for item in sparkline_list])
                #sparkline_label = sparkline_font.render(sparkline_text, True, text_color)
                
                # temperature, humidity, and pressure
                other_text = '%0.1f\u00b0F | %0.1f%% rH | %0.1f mbar' % (temperature, humidity, pressure) # update the temp/rH label
                #other_label = little_font.render(other_text, True, text_color)
                #other_text_width = little_font.size(other_text)
               
                # push updates to screen
                
                window['co2'].update(value = str(co2_text), text_color = t_color)
                window.refresh()
                window['ppm'].update(text_color = t_color)
                window.refresh()
                window['sparkline'].update(value = str(sparkline_text), text_color = t_color)
                window.refresh()
                window['other'].update(value = str(other_text), text_color = t_color)
                window.refresh()

                # update current time for logfiles
                update_time()

                if logging == True:
                    write_log()

                time_last = int(time.monotonic()) # update time we last polled the CO2 sensor
    except:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        error_file = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        print("Hmmm... that didn't work as expected.")
        print("Exception: ", exception_type)
        print("File: ", error_file)
        print("Line: ", line_number)
    finally: 
        pass
    if repeat == False:
        # turn off the LEDs and exit
        ledshim.set_all(0, 0, 0)
        ledshim.show()
        window.close()
        os._exit(1)
        break
    else:
        continue
    os._exit(1)
    break
 
if debug == True:
    print("Exiting")

#pygame.quit()
sys.exit()


