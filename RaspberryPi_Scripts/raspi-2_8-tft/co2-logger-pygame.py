# Sensor Logging and Display Script 
# by Cordelya Sharpe
# based on, in part, several examples at https://learn.adafruit.com
# Raspberry Pi + Official 7" Touchscreen Edition
# Licensed under the MIT License. See https://gitlab.com/cordelya/co2-sensor-display

import time
import datetime
import board
import busio
import adafruit_scd30
import adafruit_lps2x
import adafruit_pcf8523
import os
import sys
import getopt
import ledshim
import pygame
import sparklines
import RPi.GPIO as GPIO
from pigame import PiTft

# ========== Functions ============= #

def btn_detect():
    try:
        for (k,v) in button_map.items():
            if GPIO.input(k) == False and v == 18:
                print("Exit Button Pressed!")
                global repeat
                repeat = False
                #pygame.event.post(pygame.event.Event(pygame.QUIT))
    except:
        print("Button Press Detection Failed")

def led_next(color):
    if debug == True:
        print("Step ", color)
    if color == 0:
        ledshim.set_all(209, 0, 0)
        ledshim.show()
    elif color == 1:
        ledshim.set_all(255, 102, 34)
        ledshim.show()
    elif color == 2:
        ledshim.set_all(255, 218, 33)
        ledshim.show()
    elif color == 3:
        ledshim.set_all(51, 221, 0)
        ledshim.show()
    elif color == 4:
        ledshim.set_all(17, 51, 204)
        ledshim.show()    
    elif color == 5:
        ledshim.set_all(34, 0, 102)
        ledshim.show()    
    elif color == 6:
        ledshim.set_all(51, 0, 68)
        ledshim.show() 
    else:
        ledshim.set_all(255, 255, 255)
        ledshim.show()
    color += 1
    color %= 7
    global step
    step = color
    return

# ========= Variables & Init ========== #

debug = False
logging = True
location = "unknown"
pressure = 1013.25 # the default pressure to use. 1013.25 is standard air pressure at sea level
filename = True
grafana = False # grafana logging set to false by default. To turn on, pass a location using -l [location] when starting this script
logfile_path = "/home/pi/logs/" # set this to the full path of where you want logfiles to live

# init the LED SHIM because we'll use it to indicate load progress
ledshim.set_clear_on_exit()
step = 7
led_next(step)


# init the exit button
# GPIO 18 will be used for the exit button. Remaining 3 buttons remain unprogrammed, available for other uses
button_map = {23:23, 22:22, 27:27, 18:18}
GPIO.setmode(GPIO.BCM)
for k in button_map.keys():
    GPIO.setup(k, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# it is possible to pass in some arguments when you invoke the script
# -d [True|False] will override the debug flag to whichever boolean you supply
# -f [filename] will override the default file naming convention
# -g [True|False] will override the logging flag. Can be overridden if the script is unable to initialize a log file.
# -l [location] will set location to the arg and grafana to True - turns on grafana logging
# -p [####.###] Supply an override air pressure expressed as a decimal. 
#   Supplying more than 6.3 digits won't break this script, but it will be rejected by my Grafana database table for this data.
print(sys.argv)
try:
    opts, args = getopt.getopt(sys.argv[1:], "d:f:g:l:p:")
    
except:
    print("Unable to get args. Will continue with script defaults.")
finally:
    for opt, arg in opts:
        if opt in ['-d']:
            debug = bool(arg)
            print("Debug flag =", debug)
        if opt in ['-f']:
            filename = arg.strip(" ")
            print("Filename set to", filename)
        if opt in ['-g']:
            logging = bool(arg) #script is set to log if able to initialize a new file. This will turn the flag back off.
            print("Logging turned OFF")
            print("Logging flag =", logging)
        if opt in ['-l']:
            location = arg.strip(" ")
            grafana = True
            print("Location set to", location)
            print("Grafana logging flag set to", grafana)
        if opt in ['-p']:
            pressure = int(arg)
            print("Pressure set to", pressure) # this can still be overridden by an attached pressure sensor
    if debug == True:
        print("Done getting args")
print("Debug", debug, ", filename", filename, ", logging", logging, ", location", location, ", grafana", grafana, ", pressure", pressure)
print("Setting some needed variables")

led_next(step)

# set desired CO2 ranges. The "yellow" range will be the values between the HIGH and the LOW
CO2_HIGH = 1200 # the lowest PPM of the "high" range
CO2_LOW = 800 # the highest PPM of the "low" range

# customize text [and future LED] colors
green = (3, 252, 44) # the color to use for low ("good") levels
yellow = (240, 252, 3) # the color to use for medium ("almost bad") levels
red = (252, 3, 3) # the color to use for high ("bad") levels

# set a audio files for the audible alarm to use
yellow_alarm_f = '/home/pi/Music/yellow.ogg' # this sound will loop while in yellow range
red_alarm_f = '/home/pi/Music/red.ogg' # this sound will loop while in red range

# how long should we wait between sensor reads, in seconds
DATA_REFRESH = 5

# display stuff
text_color = green # initially sets the text and color to green
DISPLAY_HEIGHT = 480 # the screen's height in px
DISPLAY_WIDTH = 640 # the screen's width in px

print("setting up last resort variables")
# if you don't have an LPS22, you can set a number of degrees C
# to subtract from SCD30, which tends to run/read a few degrees hot
temperature_delta = 0 # positive int or float of degrees to subtract from SCD30
# if you won't have Internet access or a RTC, you can estmate your start time
rtc_time = time.gmtime(1630954461) #this takes 'seconds since epoch' for your date/time. 
time_delta = False # stores time.monotonic() for later delta calculation
date_time = False # initialize for time handling
t = False # initialize for time handling
if debug == True:
    print("Variables Set.. Initializing hardware")

led_next(step)

# provide details about the screen
os.putenv('SDL_VIDEODRIVER', 'fbcon')
os.putenv('SDL_FBDEV', '/dev/fb1')
os.putenv('SDL_MOUSEDRV', 'dummy')
os.putenv('SDL_MOUSEDEV', '/dev/null')
os.putenv('DISPLAY', '')


# initialize hardware and display items
sensor = adafruit_scd30.SCD30(board.I2C())

led_next(step)

pygame.init() # initialize pygame for screen drawing
os.environ["DISPLAY"] = ':0'
pygame.display.init()

led_next(step)

pygame.mixer.init() # initialize pygame audio mixer for audible alarm
yellow_alarm = pygame.mixer.Sound(yellow_alarm_f) # loads the alarm audio file for playing
red_alarm = pygame.mixer.Sound(red_alarm_f)

led_next(step)

pygame.font.init()
pygame.mouse.set_visible(False)
big_font = pygame.font.SysFont('nunito', 216)
ppm_font = pygame.font.SysFont('nunito', 72)
sparkline_font = pygame.font.SysFont('dejavusansmono', 60)
little_font = pygame.font.SysFont('nunito', 42)

led_next(step)

ts = PiTft()
screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN) # initialize the display
screen.fill((0, 0, 0))
pygame.display.update()
pygame.fastevent.init()


led_next(step)

# timekeeping: try to get internet time, try RTC, or fail back to manually set time
# if a wireless connection is available, query the current time.
try:
    rtc_time = time.localtime()
    time_delta = time.monotonic()
except:
    print("I couldn't retrieve the current time from the Internet")
    # if RTC is available, create object and update pressure variables
    try:
        rtc = adafruit_pcf8523.PCF8523(board.I2C())
        rtc_time = rtc.datetime # expressed as struct_time
        time_delta = time.monotonic()
    except:
        time_delta = time.monotonic()
        print("I couldn't get the current time from a RTC")
finally:
    try:
        rtc = adafruit_pcf8523.PCF8523(board.I2C())
        rtc.datetime = time.localtime()
        if debug == True:
            print("Updated RTC with Internet Time")
    except:
        print("Could not update RTC with Internet Time")

led_next(step)

if debug == True:
    print("Step:", step)
    print("Preparing for data logging")

# Prepare for data logging
# Get next incremental filename here and re-assign filename
# If this fails, logging will not occur, but display will continue
g_filename = False
i = 0 # initialize filename counter
try:
    if filename == True and logging == True:
        y = 0 # counter for traversing the file list
        sd_dir = os.listdir(logfile_path) # get the list of files
        sd_dir.sort() # sort the file list so the filenames are in order
        while y < len(sd_dir): #iterate over the file list looking for filename matches
            while sd_dir[y] == ("log%03d.csv" % i):
                i += 1 # we found a match, so increment the filename counter
            y += 1 # increment the list iterator to advance to the next filename
except:
        print("Could not read the next file number")
        logging = False
filename = logfile_path + ("log%03d.csv" % i) # create the filename from the counter
if grafana == True:
    g_filename = logfile_path + ("grafana%03d.csv" % i) # create filename for grafana log

led_next(step)

try:
    with open(filename, "w+") as f:
        f.write("datetime, co2, temp, humidity, pressure\n")
    if debug == True:
        print(filename + " initialized")
except:
    print("Could not initialize regular log file.")
    logging = False
    print("Logging flag set to ", logging)
if grafana == True:
    try:
        with open(g_filename, "w+") as f:
            f.write("epoch_time, metric, location, reading \n")
        if debug == True:
            print(g_filename, " initialized")
    except:
        print("Could not initialize grafana log file")
        grafana = False

led_next(step)

if debug == True:
    print("Step:", step)
    print("Done preparing for logging")
    print("Starting pressure sensor init")

# if LPS22 is available, create object and update the SCD30
# we'll also check for a temperature difference and set a delta
try:
    pressure_sen = adafruit_lps2x.LPS22(board.I2C())
    if debug == True:
        print("Pressure Sensor Debug (pressure: %f temp: %fC)" % (pressure_sen.pressure, pressure_sen.temperature))
    sensor.ambient_pressure = pressure_sen.pressure
    pressure = pressure_sen.pressure
    temperature_delta = sensor.temperature - pressure_sen.temperature # record difference in temperature for later
except:
    sensor.ambient_pressure = pressure #average pressure at sea level; fallback default
    print("I couldn't connect to a pressure sensor, so I set the CO2 sensor's reference pressure to the default")

    if debug == True:
        print("The temperature delta is %f" % temperature_delta)
        print("Updated the CO2 sensor's pressure input")

led_next(step)

if debug == True:
    print("Done with pressure sensor init")
    print("Now setting up graphical presentation")

# set up text
co2_content = ' ' 

co2_label = big_font.render(co2_content, True, green)
co2_label_width = big_font.size(co2_content)
screen.blit(co2_label, (DISPLAY_WIDTH / 2 - co2_label_width[0] / 2, (DISPLAY_HEIGHT - co2_label_width[1]) * 0.01))

ppm = 'PPM'
ppm_label = ppm_font.render(ppm, True, green)
ppm_label_width = ppm_font.size(ppm)
screen.blit(ppm_label, (DISPLAY_WIDTH / 2 - ppm_label_width[0] / 2, (DISPLAY_HEIGHT - ppm_label_width[1]) * 0.6))


other_content = '   \u00b0F |    %rH |       mbar'
other_label_width = little_font.size(other_content)
other_label = little_font.render(other_content, True, green)
screen.blit(other_label, (DISPLAY_WIDTH / 2 - other_label_width[0] / 2, (DISPLAY_HEIGHT - other_label_width[1]) * 0.98))

pygame.display.update()

led_next(step)

if debug == True:
    print("Step:", step)
    print("Pausing for a moment")
# if we don't have a short pause here, the sketch sometimes just stops
time.sleep(2)

# init variables for the main loop
repeat = True
time_last = int(time.monotonic()) - 5
pressure_last = int(time.monotonic())
if debug == True:
    print("Ready to enter the main loop")

# init sparkline list
co2_list = []
for x in range(18):
    co2_list.append(float(0))

while True:
    
    # check for button presses
    btn_detect() 

    # wait for the sensor to report that it has a reading, then store the readings into local variables
    try:
        if (int(time.monotonic()) - time_last) >= DATA_REFRESH:
            if sensor.data_available:
                if debug == True:
                    print("Successfully met conditions for taking a reading.")
                    
                # convert the temperature reading to F, and trim to 1 decimal place
                temperature = round(((sensor.temperature - temperature_delta)* 9 / 5) + 32, 1)
                co2 = sensor.CO2 # store the CO2 reading
                co2_list.append(float(co2)) # add the CO2 Reading to the sparkline list
                if len(co2_list) > 18:
                    co2_list.pop(0) # trim the sparkline list by removing the oldest item
                humidity = round(sensor.relative_humidity, 1) # store the rH reading and trim to 1 decimal place
                try: 
                    if pressure_sen:
                        pressure = pressure_sen.pressure
                        if int(time_monotonic() - pressure_last) > 60:
                            sensor.pressure = pressure # if the last time the pressure was updated was longer than 60 seconds, update it
                        temperature = round((pressure_sen.temperature * 9 / 5) + 32, 1) # pressure sensor's temp reading is more accurate
                except:
                    if debug == True:
                        print("A pressure sensor is not connected")
                # determine what color and sound-file we should use based on CO2 concentration
                if co2 > CO2_HIGH:
                    # text color and LED
                    text_color = red
                    ledshim.set_all(red[0], red[1], red[2])
                    ledshim.show()
                    # audible alarm sound file
                    if pygame.mixer.Sound.get_num_channels(red_alarm) < 1:
                        if pygame.mixer.Sound.get_num_channels(yellow_alarm) > 0:
                            pygame.mixer.Sound.fadeout(yellow_alarm, 1000)
                        pygame.mixer.Sound.play(red_alarm, loops=-1, fade_ms=1000)
                elif co2 > CO2_LOW:
                    # text color and LED
                    text_color = yellow
                    ledshim.set_all(yellow[0], yellow[1], yellow[2])
                    ledshim.show()
                    # audible alarm sound file
                    if pygame.mixer.Sound.get_num_channels(yellow_alarm) < 1:
                        if pygame.mixer.Sound.get_num_channels(red_alarm) > 0:
                            pygame.mixer.Sound.fadeout(red_alarm, 1000)
                        pygame.mixer.Sound.play(yellow_alarm, loops=-1, fade_ms=1000)
                else:
                    # text color and LED
                    text_color = green
                    ledshim.set_all(green[0], green[1], green[2])
                    ledshim.show()
                    # audible alarm sound file
                    if pygame.mixer.Sound.get_num_channels(yellow_alarm) > 0:
                        pygame.mixer.Sound.fadeout(yellow_alarm, 1000)
                    if pygame.mixer.Sound.get_num_channels(red_alarm) > 0:
                        pygame.mixer.Sound.fadeout(red_alarm, 1000)
                
                # update the screen
                screen.fill(pygame.Color("black"))
                
                # co2 label
                co2_text = str(int(co2)) # update the co2_text label with the fresh data
                co2_label_width = big_font.size(co2_text)
                co2_label = big_font.render(co2_text, True, text_color)
                screen.blit(co2_label, (DISPLAY_WIDTH / 2 - co2_label_width[0] / 2, (DISPLAY_HEIGHT - co2_label_width[1]) * 0.01))
                
                # PPM label
                ppm_label = ppm_font.render(ppm, True, text_color)
                ppm_label_width = ppm_font.size(ppm)
                screen.blit(ppm_label, (DISPLAY_WIDTH / 2 - ppm_label_width[0] / 2, (DISPLAY_HEIGHT - ppm_label_width[1]) * 0.6))

                # sparkline with the most recent co2 readings
                sparkline_list = sparklines.sparklines(co2_list) #produces a list
                sparkline_text = " ".join([str(item) for item in sparkline_list])
                sparkline_label = sparkline_font.render(sparkline_text, True, text_color)
                screen.blit(sparkline_label, ((DISPLAY_WIDTH * 0.01), (DISPLAY_HEIGHT * 0.65)))
                
                # temperature, humidity, and pressure
                other_text = '%0.1f\u00b0F | %0.1f%% rH | %0.1f mbar' % (temperature, humidity, pressure) # update the temp/rH label
                other_label = little_font.render(other_text, True, text_color)
                other_text_width = little_font.size(other_text)
                screen.blit(other_label, (DISPLAY_WIDTH / 2 - other_text_width[0] / 2, (DISPLAY_HEIGHT - other_text_width[1]) * 0.98))
                
                # push updates to screen
                pygame.display.update()

                # update current time for logfiles
                if rtc_time != False:
                    current = (time.monotonic())
                    seconds = (current - time_delta)
                    date_time = (time.mktime(rtc_time) + int(seconds))
                    t = time.localtime(date_time)
                else:
                    date_time = time.monotonic()

                if logging == True:
                    datestring = '%04d-%02d-%02d_%02d:%02d:%02d' % (t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec)
                    with open(filename, "a") as f:
                        f.write(str(datestring) + "," + str(co2) + "," + str(temperature) + "," + str(humidity) + "," + str(pressure) +"\n")
                        if debug == True:
                            print(datestring, " ", co2, " ", temperature, " ", humidity, " ", pressure, " written to log")
                    if grafana == True: # epoch_time, metric, location, reading
                        epoch_time = time.mktime(time.localtime())
                        with open(g_filename, "a") as f:
                            f.write(str(int(epoch_time)) + ",co2," + location + "," + str(round(co2, 3)) + "\n")
                            f.write(str(int(epoch_time)) + ",temperature," + location + "," + str(round(temperature, 3)) + "\n")
                            f.write(str(int(epoch_time)) + ",humidity," + location + "," + str(round(humidity, 3)) + "\n")
                            f.write(str(int(epoch_time)) + ",pressure," + location + "," + str(round(pressure, 3)) + "\n")
                            if debug == True:
                                print(int(epoch_time), " ", location, " ", round(co2, 3), " ", round(temperature, 3), " ", round(humidity, 3), " ", round(pressure, 3), " written to Grafana log")
                time_last = int(time.monotonic()) # update time we last polled the CO2 sensor
    except:
        exception_type, exception_object, exception_traceback = sys.exc_info()
        error_file = exception_traceback.tb_frame.f_code.co_filename
        line_number = exception_traceback.tb_lineno
        print("Hmmm... that didn't work as expected.")
        print("Exception: ", exception_type)
        print("File: ", error_file)
        print("Line: ", line_number)
    finally: 
        pass
    if repeat == False:
        # turn off the LEDs and exit
        ledshim.set_all(0, 0, 0)
        ledshim.show()
        os._exit(1)
        break
    else:
        continue
    os._exit(1)
    break
 
if debug == True:
    print("Exiting")

pygame.quit()
sys.exit()


