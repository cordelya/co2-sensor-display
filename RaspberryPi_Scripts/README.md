# Using a Raspberry Pi

You can use a Raspberry Pi to run these scripts. In all cases, you will still at minimmum need one Stemma SCD30 breakout board and one way to connect it to the Raspberry Pi's I2C pins (The Stemma SHIM and a Stemma/Qwiic cable is the easiest route)

In many cases you will also need a specific display, LEDs, audio peripherals, etc. Each script's README will have details on what was used to develop/test it.

If you want to use a different display or other peripheral, it is up to you to work out how to modify the script. Feel free to create your own fork of this repository.

This script has had argument handling and grafana-specific logging added to it.

## Argument handling

You can pass this script arguments via the command line when invoking it (including via a shell script or batch file)

### Available arguments

* -d \[True|False] debug flag - True to display debug prints and False to not display debug prints
* -f \[filename] set a custom file name
* -g \[True|False] logging flag - True to log and False to not log
* -l \[location] set a custom location - this can be anything you'd like. It's for Grafana logging, so setting this will enable Grafana logging
* -p \[decimal] set a custom pressure value - if you don't have a pressure sensor but do know the local pressure, you can manually pass it in

### Example

`python3 co2-logger.py -d True -f testing001.csv -l home`

## Grafana logging

I wanted to be able to easily view my logged data using Grafana, so I did some things to make that easier

* Either set the grafana variable to True in the script, or include `-g True` when you invoke the script to enable Grafana logging
* A separate file with "Grafana" in the filename will be initialized.
* Readings will be written into this file with each metric on a separate line, and the date expressed as "seconds since \[Linux] epoch"
* See the `extras` folder in the main repo for database structure information and data loading instructions, as well as a json file with an example Grafana dashboard you can use to re-create the dashboard view shown in the top-level folder of this repository.
