# Extras!

## Grafana Dashboard Template

The file `grafana-dash.json` contains what you need to set up a dashboard like the one on the top-level readme in this repo. This assumes that your data is stored in a MySQL database named `grafana` and that you've got a table, `co2_logs`, with the following structure:

- epoch_time INT([enough INT places to hold 'seconds since Linux epoch' values])
- metric VARCHAR(20)
- location VARCHAR(20)
- reading DECIMAL(6.3)

You will need to rearrange data in regular log files before importing into the DB, but if you're running a version of the script that also outputs log files intended for grafana, those files will be ready to import with no modification. Use:

```
LOAD DATA LOCAL INFILE '/home/path/to/grafana.csv' INTO TABLE co2_logs FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;
```

If anything about your database is different, you'll need to modify things to fit. Once you have one dashboard set up, you can clone it (use "Save As" in the dashboard settings menu), and then modify the metric selector expression in the `WHERE` clause on each panel.
