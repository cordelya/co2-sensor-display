# CO2 Sensor Display

A collection of python scripts for use with CircuitPython-compatible devices and a compatible CO2 sensor. Displays the sensed CO2 in ppm along with the sensed temperature and relative humidity. Some scripts do even more than that.

Originally written for PyPortal, other specialized versions may be added in the future. If you're not using the same hardware, you will need to tweak the script. Script versions will each be placed in their own folder with their own README file arranged by what hardware it supports or needs to function properly. This document will include a list to make it easier to find the script you want.

## [PyPortal and other Circuit-python boards](https://gitlab.com/cordelya/co2-sensor-display/-/tree/main/PyPortal%20Scripts)

* [The Soup-to-Nuts Sketch](https://gitlab.com/cordelya/co2-sensor-display/-/tree/main/PyPortal%20Scripts/wifi_scd30_pcf8523_lps2x)
    * Internet time, pressure, temperature, humidity, CO2, & SD card data logging
    * Should fail gracefully on any absent externals
* [The Soup-to-Nuts minus Wifi Sketch](https://gitlab.com/cordelya/co2-sensor-display/-/tree/main/PyPortal%20Scripts/scd30_lps2x_pcf8523_sd-log)
    * Just like the Soup-to-Nuts Sketch but it doesn't try to get Internet time, so it's faster
    * Should fail gracefully on any absent externals
* [The Barebones SCD30-Only Sketch](https://gitlab.com/cordelya/co2-sensor-display/-/tree/main/PyPortal%20Scripts/SCD-30_only)
    * When all you have is the PyPortal and the SCD30, this will show the readings and change the colors and that's it.

## Raspberry Pi Boards

* [Raspberry Pi plus Official 7" Touchscreen](https://gitlab.com/cordelya/co2-sensor-display/-/tree/main/RaspberryPi_Scripts/raspi-7-inch-touchscreen)
    * All the bells and whistles (lights *and* sounds, provided you have the right hardware)
    * Should fail gracefully on absent externals (TODO: make pressure sensor absence fail gracefully).
* [Raspberry Pi with PiTFT 2.8" Touchscreen](https://gitlab.com/cordelya/co2-sensor-display/-/tree/main/RaspberryPi_Scripts/raspi-2_8-tft)
  * 
----

## Then What?

After you log all that data, you can put it into a MySQL database and display it in Grafana, like so:

![Four charts depicting sample log data of Air Pressure, Temperature, CO2, and Humidity](/source/images/co2-log-graphs-grafana.jpg "Grafana Example")
There's information on this in the `extras` directory.

Or, you can use a Jupyter Notebook to visualize it. Head over to my [co2-sensor-notebooks](https://gitlab.com/cordelya/co2-sensor-notebooks) repository to find some examples and a link to a demo.
